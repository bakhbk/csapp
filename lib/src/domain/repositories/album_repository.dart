import 'package:csapp/src/core/params/request_params.dart';
import 'package:csapp/src/core/resources/data_state.dart';
import 'package:csapp/src/domain/entities/album.dart';

abstract class AlbumsRepository {
  Future<DataState<List<Album>>> getAlbumList();

  Future<DataState<List<Album>>> getAlbumsByUserId(AlbumsRequestParams params);
}
