import 'package:csapp/src/core/resources/data_state.dart';
import 'package:csapp/src/domain/entities/user.dart';

abstract class UsersRepository {
  Future<DataState<List<User>>> getUsers();

  Future<DataState<User>> getUserById(int userId);
}
