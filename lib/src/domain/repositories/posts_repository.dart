import 'package:csapp/src/core/resources/data_state.dart';
import 'package:csapp/src/domain/entities/comment.dart';
import 'package:csapp/src/domain/entities/post.dart';

abstract class PostsRepository {
  Future<DataState<List<Post>>> getPosts();

  Future<DataState<List<Post>>> getPostsByUserId(int userId);

  Future<DataState<List<Comment>>> getCommentsByPostId(int postId);

  Future<DataState<List<Comment>>> sendComment(Comment comment);
}
