import 'dart:convert';

class MapConverter {
  const MapConverter();

  Map<String, dynamic> stringToMap(String? fromDb) {
    if (fromDb is String && fromDb.isNotEmpty) {
      return jsonDecode(fromDb) as Map<String, dynamic>;
    } else {
      return {};
    }
  }

  String mapToString(Map? value) {
    try {
      if (value is Map && value.isNotEmpty) {
        return jsonEncode(value);
      }
      return '';
    } catch (_) {
      return '';
    }
  }
}
