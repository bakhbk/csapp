import 'package:csapp/src/core/resources/data_state.dart';
import 'package:csapp/src/core/use_case/use_case.dart';
import 'package:csapp/src/data/repositories/users_repository_impl.dart';
import 'package:csapp/src/domain/entities/user.dart';
import 'package:injectable/injectable.dart';

@lazySingleton
class GetUsersUseCase implements UseCase<DataState<List<User>>, void> {
  final UsersRepositoryImpl _repository;

  GetUsersUseCase(this._repository);

  @override
  Future<DataState<List<User>>> call([_]) {
    return _repository.getUsers();
  }
}
