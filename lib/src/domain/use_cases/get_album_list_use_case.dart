import 'package:csapp/src/core/resources/data_state.dart';
import 'package:csapp/src/core/use_case/use_case.dart';
import 'package:csapp/src/data/repositories/albums_repository_impl.dart';
import 'package:csapp/src/domain/entities/album.dart';
import 'package:injectable/injectable.dart';

@lazySingleton
class GetAlbumsUseCase implements UseCase<DataState<List<Album>>, void> {
  final AlbumsRepositoryImpl _repository;

  GetAlbumsUseCase(this._repository);

  @override
  Future<DataState<List<Album>>> call([_]) {
    return _repository.getAlbumList();
  }
}
