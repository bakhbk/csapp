import 'package:csapp/src/core/resources/data_state.dart';
import 'package:csapp/src/core/use_case/use_case.dart';
import 'package:csapp/src/data/repositories/posts_repository_impl.dart';
import 'package:csapp/src/domain/entities/comment.dart';
import 'package:injectable/injectable.dart';

@lazySingleton
class SendCommentUseCase implements UseCase<DataState<List<Comment>>, Comment> {
  final PostsRepositoryImpl _repository;

  SendCommentUseCase(this._repository);

  @override
  Future<DataState<List<Comment>>> call(Comment comment) {
    return _repository.sendComment(comment);
  }
}
