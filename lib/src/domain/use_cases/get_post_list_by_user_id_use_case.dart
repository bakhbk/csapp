import 'package:csapp/src/core/resources/data_state.dart';
import 'package:csapp/src/core/use_case/use_case.dart';
import 'package:csapp/src/data/repositories/posts_repository_impl.dart';
import 'package:csapp/src/domain/entities/post.dart';
import 'package:injectable/injectable.dart';

@lazySingleton
class GetPostsByUserIdUseCase implements UseCase<DataState<List<Post>>, int> {
  final PostsRepositoryImpl _repository;

  GetPostsByUserIdUseCase(this._repository);

  @override
  Future<DataState<List<Post>>> call(int userId) {
    return _repository.getPostsByUserId(userId);
  }
}
