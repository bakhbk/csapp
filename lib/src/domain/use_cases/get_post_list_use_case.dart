import 'package:csapp/src/core/resources/data_state.dart';
import 'package:csapp/src/core/use_case/use_case.dart';
import 'package:csapp/src/data/repositories/posts_repository_impl.dart';
import 'package:csapp/src/domain/entities/post.dart';
import 'package:injectable/injectable.dart';

@lazySingleton
class GetPostListUseCase implements UseCase<DataState<List<Post>>, void> {
  final PostsRepositoryImpl _repository;

  GetPostListUseCase(this._repository);

  @override
  Future<DataState<List<Post>>> call([_]) {
    return _repository.getPosts();
  }
}
