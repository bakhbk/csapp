import 'package:csapp/src/core/resources/data_state.dart';
import 'package:csapp/src/core/use_case/use_case.dart';
import 'package:csapp/src/data/repositories/posts_repository_impl.dart';
import 'package:csapp/src/domain/entities/comment.dart';
import 'package:injectable/injectable.dart';

@lazySingleton
class GetCommentsByPostIdUseCase
    implements UseCase<DataState<List<Comment>>, int> {
  final PostsRepositoryImpl _repository;

  GetCommentsByPostIdUseCase(this._repository);

  @override
  Future<DataState<List<Comment>>> call(int postId) {
    return _repository.getCommentsByPostId(postId);
  }
}
