import 'package:csapp/src/core/params/request_params.dart';
import 'package:csapp/src/core/resources/data_state.dart';
import 'package:csapp/src/core/use_case/use_case.dart';
import 'package:csapp/src/data/repositories/albums_repository_impl.dart';
import 'package:csapp/src/domain/entities/album.dart';
import 'package:injectable/injectable.dart';

@lazySingleton
class GetAlbumsByUserIdUseCase
    implements UseCase<DataState<List<Album>>, AlbumsRequestParams> {
  final AlbumsRepositoryImpl _repository;

  GetAlbumsByUserIdUseCase(this._repository);

  @override
  Future<DataState<List<Album>>> call(AlbumsRequestParams params) {
    return _repository.getAlbumsByUserId(params);
  }
}
