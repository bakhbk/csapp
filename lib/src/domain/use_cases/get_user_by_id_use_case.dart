import 'package:csapp/src/core/resources/data_state.dart';
import 'package:csapp/src/core/use_case/use_case.dart';
import 'package:csapp/src/data/repositories/users_repository_impl.dart';
import 'package:csapp/src/domain/entities/user.dart';
import 'package:injectable/injectable.dart';

@lazySingleton
class GetUserByIdUseCase implements UseCase<DataState<User>, int> {
  final UsersRepositoryImpl _repository;

  GetUserByIdUseCase(this._repository);

  @override
  Future<DataState<User>> call(int userId) {
    return _repository.getUserById(userId);
  }
}
