import 'package:csapp/src/domain/entities/photo.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'album.freezed.dart';

part 'album.g.dart';

/// {
/// "userId": 1,
/// "id": 1,
/// "title": "quidem molestiae enim"
/// }

@freezed
class Album with _$Album {
  const factory Album({
    required int id,
    required int userId,
    @Default('') String title,
    @Default([]) List<Photo> photos,
  }) = _Album;

  factory Album.fromJson(Map<String, dynamic> json) => _$AlbumFromJson(json);
}
