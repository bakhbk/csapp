import 'package:freezed_annotation/freezed_annotation.dart';

part 'user.freezed.dart';

part 'user.g.dart';

///```json
/// {
///     "id": 1,
///     "name": "Leanne Graham",
///     "username": "Bret",
///     "email": "Sincere@april.biz",
///     "address": {
///       "street": "Kulas Light",
///       "suite": "Apt. 556",
///       "city": "Gwenborough",
///       "zipcode": "92998-3874",
///       "geo": {
///         "lat": "-37.3159",
///         "lng": "81.1496"
///       }
///     },
///     "phone": "1-770-736-8031 x56442",
///     "website": "hildegard.org",
///     "company": {
///       "name": "Romaguera-Crona",
///       "catchPhrase": "Multi-layered client-server neural-net",
///       "bs": "harness real-time e-markets"
///     }
/// }
///```
@freezed
class User with _$User {
  const factory User({
    required int id,
    @Default('') @Default('') String name,
    @Default('') String username,
    @Default('') String email,
    @Default('') String phone,
    @Default('') String website,
    @JsonKey(name: 'address') Address? address,
    @JsonKey(name: 'company') Company? company,
  }) = _User;

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);
}

///     "address": {
///       "street": "Kulas Light",
///       "suite": "Apt. 556",
///       "city": "Gwenborough",
///       "zipcode": "92998-3874",
///       "geo": {
///         "lat": "-37.3159",
///         "lng": "81.1496"
///       }
///     },
///
@freezed
class Address with _$Address {
  const factory Address({
    @Default('') String street,
    @Default('') String suite,
    @Default('') String city,
    @Default('') String zipcode,
    @JsonKey(name: 'geo') Geo? geo,
  }) = _Address;

  factory Address.fromJson(Map<String, dynamic> json) =>
      _$AddressFromJson(json);
}

///       "geo": {
///         "lat": "-37.3159",
///         "lng": "81.1496"
///       }
@freezed
class Geo with _$Geo {
  const factory Geo({
    @Default('') String lat,
    @Default('') String lng,
  }) = _Geo;

  factory Geo.fromJson(Map<String, dynamic> json) => _$GeoFromJson(json);
}

///     "company": {
///       "name": "Romaguera-Crona",
///       "catchPhrase": "Multi-layered client-server neural-net",
///       "bs": "harness real-time e-markets"
///     }
@freezed
class Company with _$Company {
  const factory Company({
    @Default('') String name,
    @Default('') String catchPhrase,
    @Default('') String bs,
  }) = _Company;

  factory Company.fromJson(Map<String, dynamic> json) =>
      _$CompanyFromJson(json);
}
