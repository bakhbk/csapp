import 'package:freezed_annotation/freezed_annotation.dart';

part 'photo.freezed.dart';

part 'photo.g.dart';

/// {
///     "albumId": 2,
///     "id": 51,
///     "title": "non sunt voluptatem placeat consequuntur rem incidunt",
///     "url": "https://via.placeholder.com/600/8e973b",
///     "thumbnailUrl": "https://via.placeholder.com/150/8e973b"
///   }

@freezed
class Photo with _$Photo {
  const factory Photo({
    required int albumId,
    required int id,
    @Default('') String title,
    @Default('') String url,
    @Default('') String thumbnailUrl,
  }) = _Photo;

  factory Photo.fromJson(Map<String, dynamic> json) => _$PhotoFromJson(json);
}
