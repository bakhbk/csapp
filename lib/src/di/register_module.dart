import 'package:csapp/src/config/routes/routes.gr.dart';
import 'package:csapp/src/data/data_sources/remote/api_service.dart';
import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:logger/logger.dart';
import 'package:shared_preferences/shared_preferences.dart';

@module
abstract class RegisterModule {
  @singleton
  Logger get logger => Logger();

  @singleton
  ApiService get client => ApiService(Dio());

  @singleton
  AppRouter get router => AppRouter();

  @singleton
  @preResolve
  Future<SharedPreferences> get prefs => SharedPreferences.getInstance();
}
