// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;
import 'package:logger/logger.dart' as _i6;
import 'package:shared_preferences/shared_preferences.dart' as _i7;

import '../config/routes/routes.gr.dart' as _i5;
import '../data/data_sources/local/app_prefs.dart' as _i9;
import '../data/data_sources/remote/api_service.dart' as _i4;
import '../data/repositories/albums_repository_impl.dart' as _i17;
import '../data/repositories/posts_repository_impl.dart' as _i13;
import '../data/repositories/users_repository_impl.dart' as _i8;
import '../domain/use_cases/get_album_list_use_case.dart' as _i19;
import '../domain/use_cases/get_albums_by_user_id_use_case.dart' as _i18;
import '../domain/use_cases/get_comments_by_post_id_use_case.dart' as _i20;
import '../domain/use_cases/get_post_list_by_user_id_use_case.dart' as _i22;
import '../domain/use_cases/get_post_list_use_case.dart' as _i21;
import '../domain/use_cases/get_user_by_id_use_case.dart' as _i10;
import '../domain/use_cases/get_users_use_case.dart' as _i11;
import '../domain/use_cases/send_comment_use_case.dart' as _i14;
import '../presentation/album/album_bloc.dart' as _i3;
import '../presentation/albums/albums_bloc.dart' as _i26;
import '../presentation/post/comments/comments_bloc.dart' as _i27;
import '../presentation/post/post_bloc.dart' as _i12;
import '../presentation/posts/posts_bloc.dart' as _i24;
import '../presentation/user/components/album/album_preview_bloc.dart' as _i25;
import '../presentation/user/components/post/post_preview_bloc.dart' as _i23;
import '../presentation/user/user_bloc.dart' as _i15;
import '../presentation/users/users_bloc.dart' as _i16;
import 'register_module.dart' as _i28; // ignore_for_file: unnecessary_lambdas

// ignore_for_file: lines_longer_than_80_chars
/// initializes the registration of provided dependencies inside of [GetIt]
Future<_i1.GetIt> $initGetIt(_i1.GetIt get,
    {String? environment, _i2.EnvironmentFilter? environmentFilter}) async {
  final gh = _i2.GetItHelper(get, environment, environmentFilter);
  final registerModule = _$RegisterModule();
  gh.factory<_i3.AlbumBloc>(() => _i3.AlbumBloc());
  gh.singleton<_i4.ApiService>(registerModule.client);
  gh.singleton<_i5.AppRouter>(registerModule.router);
  gh.singleton<_i6.Logger>(registerModule.logger);
  await gh.singletonAsync<_i7.SharedPreferences>(() => registerModule.prefs,
      preResolve: true);
  gh.lazySingleton<_i8.UsersRepositoryImpl>(
      () => _i8.UsersRepositoryImpl(get<_i4.ApiService>()));
  gh.lazySingleton<_i9.AppPrefs>(
      () => _i9.AppPrefs(get<_i7.SharedPreferences>()));
  gh.lazySingleton<_i10.GetUserByIdUseCase>(
      () => _i10.GetUserByIdUseCase(get<_i8.UsersRepositoryImpl>()));
  gh.lazySingleton<_i11.GetUsersUseCase>(
      () => _i11.GetUsersUseCase(get<_i8.UsersRepositoryImpl>()));
  gh.factory<_i12.PostBloc>(
      () => _i12.PostBloc(get<_i10.GetUserByIdUseCase>()));
  gh.lazySingleton<_i13.PostsRepositoryImpl>(() =>
      _i13.PostsRepositoryImpl(get<_i4.ApiService>(), get<_i9.AppPrefs>()));
  gh.lazySingleton<_i14.SendCommentUseCase>(
      () => _i14.SendCommentUseCase(get<_i13.PostsRepositoryImpl>()));
  gh.factory<_i15.UserBloc>(
      () => _i15.UserBloc(get<_i10.GetUserByIdUseCase>()));
  gh.factory<_i16.UsersBloc>(
      () => _i16.UsersBloc(get<_i11.GetUsersUseCase>(), get<_i5.AppRouter>()));
  gh.lazySingleton<_i17.AlbumsRepositoryImpl>(() =>
      _i17.AlbumsRepositoryImpl(get<_i4.ApiService>(), get<_i9.AppPrefs>()));
  gh.lazySingleton<_i18.GetAlbumsByUserIdUseCase>(
      () => _i18.GetAlbumsByUserIdUseCase(get<_i17.AlbumsRepositoryImpl>()));
  gh.lazySingleton<_i19.GetAlbumsUseCase>(
      () => _i19.GetAlbumsUseCase(get<_i17.AlbumsRepositoryImpl>()));
  gh.lazySingleton<_i20.GetCommentsByPostIdUseCase>(
      () => _i20.GetCommentsByPostIdUseCase(get<_i13.PostsRepositoryImpl>()));
  gh.lazySingleton<_i21.GetPostListUseCase>(
      () => _i21.GetPostListUseCase(get<_i13.PostsRepositoryImpl>()));
  gh.lazySingleton<_i22.GetPostsByUserIdUseCase>(
      () => _i22.GetPostsByUserIdUseCase(get<_i13.PostsRepositoryImpl>()));
  gh.factory<_i23.PostPreviewBloc>(() => _i23.PostPreviewBloc(
      get<_i22.GetPostsByUserIdUseCase>(), get<_i5.AppRouter>()));
  gh.factory<_i24.PostsBloc>(() => _i24.PostsBloc(
      get<_i21.GetPostListUseCase>(),
      get<_i22.GetPostsByUserIdUseCase>(),
      get<_i5.AppRouter>()));
  gh.factory<_i25.AlbumPreviewBloc>(() => _i25.AlbumPreviewBloc(
      get<_i18.GetAlbumsByUserIdUseCase>(), get<_i5.AppRouter>()));
  gh.factory<_i26.AlbumsBloc>(() => _i26.AlbumsBloc(
      get<_i19.GetAlbumsUseCase>(),
      get<_i18.GetAlbumsByUserIdUseCase>(),
      get<_i5.AppRouter>()));
  gh.factory<_i27.CommentsBloc>(() => _i27.CommentsBloc(
      get<_i20.GetCommentsByPostIdUseCase>(), get<_i14.SendCommentUseCase>()));
  return get;
}

class _$RegisterModule extends _i28.RegisterModule {}
