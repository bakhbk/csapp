// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************
//
// ignore_for_file: type=lint

import 'package:auto_route/auto_route.dart' as _i2;
import 'package:flutter/material.dart' as _i9;

import '../../domain/entities/album.dart' as _i11;
import '../../domain/entities/post.dart' as _i10;
import '../../presentation/album/album_view.dart' as _i8;
import '../../presentation/albums/albums_view.dart' as _i6;
import '../../presentation/home/home_screen.dart' as _i1;
import '../../presentation/post/post_view.dart' as _i7;
import '../../presentation/posts/posts_view.dart' as _i5;
import '../../presentation/user/user_view.dart' as _i4;
import '../../presentation/users/users_view.dart' as _i3;

class AppRouter extends _i2.RootStackRouter {
  AppRouter([_i9.GlobalKey<_i9.NavigatorState>? navigatorKey])
      : super(navigatorKey);

  @override
  final Map<String, _i2.PageFactory> pagesMap = {
    HomeScreenRouter.name: (routeData) {
      return _i2.MaterialPageX<dynamic>(
          routeData: routeData, child: const _i1.HomeScreen());
    },
    UsersPageRouter.name: (routeData) {
      return _i2.MaterialPageX<dynamic>(
          routeData: routeData, child: const _i2.EmptyRouterPage());
    },
    AlbumsPageRouter.name: (routeData) {
      return _i2.MaterialPageX<dynamic>(
          routeData: routeData, child: const _i2.EmptyRouterPage());
    },
    PostsPageRouter.name: (routeData) {
      return _i2.MaterialPageX<dynamic>(
          routeData: routeData, child: const _i2.EmptyRouterPage());
    },
    UsersPageRoute.name: (routeData) {
      return _i2.MaterialPageX<dynamic>(
          routeData: routeData, child: _i3.UsersPage());
    },
    UserPageRoute.name: (routeData) {
      final args = routeData.argsAs<UserPageRouteArgs>();
      return _i2.MaterialPageX<dynamic>(
          routeData: routeData,
          child: _i4.UserPage(userId: args.userId, key: args.key));
    },
    PostsPageRoute.name: (routeData) {
      final args = routeData.argsAs<PostsPageRouteArgs>();
      return _i2.MaterialPageX<dynamic>(
          routeData: routeData,
          child: _i5.PostsPage(userId: args.userId, key: args.key));
    },
    AlbumsPageRoute.name: (routeData) {
      final args = routeData.argsAs<AlbumsPageRouteArgs>();
      return _i2.MaterialPageX<dynamic>(
          routeData: routeData,
          child: _i6.AlbumsPage(userId: args.userId, key: args.key));
    },
    PostPageRoute.name: (routeData) {
      final args = routeData.argsAs<PostPageRouteArgs>();
      return _i2.MaterialPageX<dynamic>(
          routeData: routeData,
          child: _i7.PostPage(post: args.post, key: args.key));
    },
    AlbumPageRoute.name: (routeData) {
      final args = routeData.argsAs<AlbumPageRouteArgs>();
      return _i2.MaterialPageX<dynamic>(
          routeData: routeData,
          child: _i8.AlbumPage(album: args.album, key: args.key));
    },
    RootAlbumsPageRoute.name: (routeData) {
      return _i2.MaterialPageX<dynamic>(
          routeData: routeData, child: const _i6.RootAlbumsPage());
    },
    RootPostsPageRoute.name: (routeData) {
      return _i2.MaterialPageX<dynamic>(
          routeData: routeData, child: const _i5.RootPostsPage());
    }
  };

  @override
  List<_i2.RouteConfig> get routes => [
        _i2.RouteConfig(HomeScreenRouter.name, path: '/', children: [
          _i2.RouteConfig(UsersPageRouter.name,
              path: 'users',
              parent: HomeScreenRouter.name,
              children: [
                _i2.RouteConfig(UsersPageRoute.name,
                    path: '', parent: UsersPageRouter.name),
                _i2.RouteConfig(UserPageRoute.name,
                    path: 'user-page', parent: UsersPageRouter.name),
                _i2.RouteConfig(PostsPageRoute.name,
                    path: 'posts-page', parent: UsersPageRouter.name),
                _i2.RouteConfig(AlbumsPageRoute.name,
                    path: 'albums-page', parent: UsersPageRouter.name),
                _i2.RouteConfig(PostPageRoute.name,
                    path: 'post-page', parent: UsersPageRouter.name),
                _i2.RouteConfig(AlbumPageRoute.name,
                    path: 'album-page', parent: UsersPageRouter.name)
              ]),
          _i2.RouteConfig(AlbumsPageRouter.name,
              path: 'albums',
              parent: HomeScreenRouter.name,
              children: [
                _i2.RouteConfig(RootAlbumsPageRoute.name,
                    path: '', parent: AlbumsPageRouter.name),
                _i2.RouteConfig(AlbumPageRoute.name,
                    path: 'album-page', parent: AlbumsPageRouter.name)
              ]),
          _i2.RouteConfig(PostsPageRouter.name,
              path: 'posts',
              parent: HomeScreenRouter.name,
              children: [
                _i2.RouteConfig(RootPostsPageRoute.name,
                    path: '', parent: PostsPageRouter.name),
                _i2.RouteConfig(PostPageRoute.name,
                    path: 'post-page', parent: PostsPageRouter.name)
              ])
        ])
      ];
}

/// generated route for
/// [_i1.HomeScreen]
class HomeScreenRouter extends _i2.PageRouteInfo<void> {
  const HomeScreenRouter({List<_i2.PageRouteInfo>? children})
      : super(HomeScreenRouter.name, path: '/', initialChildren: children);

  static const String name = 'HomeScreenRouter';
}

/// generated route for
/// [_i2.EmptyRouterPage]
class UsersPageRouter extends _i2.PageRouteInfo<void> {
  const UsersPageRouter({List<_i2.PageRouteInfo>? children})
      : super(UsersPageRouter.name, path: 'users', initialChildren: children);

  static const String name = 'UsersPageRouter';
}

/// generated route for
/// [_i2.EmptyRouterPage]
class AlbumsPageRouter extends _i2.PageRouteInfo<void> {
  const AlbumsPageRouter({List<_i2.PageRouteInfo>? children})
      : super(AlbumsPageRouter.name, path: 'albums', initialChildren: children);

  static const String name = 'AlbumsPageRouter';
}

/// generated route for
/// [_i2.EmptyRouterPage]
class PostsPageRouter extends _i2.PageRouteInfo<void> {
  const PostsPageRouter({List<_i2.PageRouteInfo>? children})
      : super(PostsPageRouter.name, path: 'posts', initialChildren: children);

  static const String name = 'PostsPageRouter';
}

/// generated route for
/// [_i3.UsersPage]
class UsersPageRoute extends _i2.PageRouteInfo<void> {
  const UsersPageRoute() : super(UsersPageRoute.name, path: '');

  static const String name = 'UsersPageRoute';
}

/// generated route for
/// [_i4.UserPage]
class UserPageRoute extends _i2.PageRouteInfo<UserPageRouteArgs> {
  UserPageRoute({required int userId, _i9.Key? key})
      : super(UserPageRoute.name,
            path: 'user-page',
            args: UserPageRouteArgs(userId: userId, key: key));

  static const String name = 'UserPageRoute';
}

class UserPageRouteArgs {
  const UserPageRouteArgs({required this.userId, this.key});

  final int userId;

  final _i9.Key? key;

  @override
  String toString() {
    return 'UserPageRouteArgs{userId: $userId, key: $key}';
  }
}

/// generated route for
/// [_i5.PostsPage]
class PostsPageRoute extends _i2.PageRouteInfo<PostsPageRouteArgs> {
  PostsPageRoute({required int userId, _i9.Key? key})
      : super(PostsPageRoute.name,
            path: 'posts-page',
            args: PostsPageRouteArgs(userId: userId, key: key));

  static const String name = 'PostsPageRoute';
}

class PostsPageRouteArgs {
  const PostsPageRouteArgs({required this.userId, this.key});

  final int userId;

  final _i9.Key? key;

  @override
  String toString() {
    return 'PostsPageRouteArgs{userId: $userId, key: $key}';
  }
}

/// generated route for
/// [_i6.AlbumsPage]
class AlbumsPageRoute extends _i2.PageRouteInfo<AlbumsPageRouteArgs> {
  AlbumsPageRoute({required int userId, _i9.Key? key})
      : super(AlbumsPageRoute.name,
            path: 'albums-page',
            args: AlbumsPageRouteArgs(userId: userId, key: key));

  static const String name = 'AlbumsPageRoute';
}

class AlbumsPageRouteArgs {
  const AlbumsPageRouteArgs({required this.userId, this.key});

  final int userId;

  final _i9.Key? key;

  @override
  String toString() {
    return 'AlbumsPageRouteArgs{userId: $userId, key: $key}';
  }
}

/// generated route for
/// [_i7.PostPage]
class PostPageRoute extends _i2.PageRouteInfo<PostPageRouteArgs> {
  PostPageRoute({required _i10.Post post, _i9.Key? key})
      : super(PostPageRoute.name,
            path: 'post-page', args: PostPageRouteArgs(post: post, key: key));

  static const String name = 'PostPageRoute';
}

class PostPageRouteArgs {
  const PostPageRouteArgs({required this.post, this.key});

  final _i10.Post post;

  final _i9.Key? key;

  @override
  String toString() {
    return 'PostPageRouteArgs{post: $post, key: $key}';
  }
}

/// generated route for
/// [_i8.AlbumPage]
class AlbumPageRoute extends _i2.PageRouteInfo<AlbumPageRouteArgs> {
  AlbumPageRoute({required _i11.Album album, _i9.Key? key})
      : super(AlbumPageRoute.name,
            path: 'album-page',
            args: AlbumPageRouteArgs(album: album, key: key));

  static const String name = 'AlbumPageRoute';
}

class AlbumPageRouteArgs {
  const AlbumPageRouteArgs({required this.album, this.key});

  final _i11.Album album;

  final _i9.Key? key;

  @override
  String toString() {
    return 'AlbumPageRouteArgs{album: $album, key: $key}';
  }
}

/// generated route for
/// [_i6.RootAlbumsPage]
class RootAlbumsPageRoute extends _i2.PageRouteInfo<void> {
  const RootAlbumsPageRoute() : super(RootAlbumsPageRoute.name, path: '');

  static const String name = 'RootAlbumsPageRoute';
}

/// generated route for
/// [_i5.RootPostsPage]
class RootPostsPageRoute extends _i2.PageRouteInfo<void> {
  const RootPostsPageRoute() : super(RootPostsPageRoute.name, path: '');

  static const String name = 'RootPostsPageRoute';
}
