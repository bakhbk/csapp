import 'package:auto_route/auto_route.dart';
import 'package:csapp/src/presentation/album/album_view.dart';
import 'package:csapp/src/presentation/albums/albums_view.dart';
import 'package:csapp/src/presentation/home/home_screen.dart';
import 'package:csapp/src/presentation/post/post_view.dart';
import 'package:csapp/src/presentation/posts/posts_view.dart';
import 'package:csapp/src/presentation/user/user_view.dart';
import 'package:csapp/src/presentation/users/users_view.dart';
import 'package:flutter/material.dart';

@MaterialAutoRouter(
  routes: <AutoRoute>[
    AutoRoute(
      path: Navigator.defaultRouteName,
      name: 'HomeScreenRouter',
      page: HomeScreen,
      children: [
        AutoRoute(
          path: 'users',
          name: 'UsersPageRouter',
          page: EmptyRouterPage,
          children: [
            AutoRoute(path: '', page: UsersPage),
            AutoRoute(page: UserPage),
            AutoRoute(page: PostsPage),
            AutoRoute(page: AlbumsPage),
            AutoRoute(page: PostPage),
            AutoRoute(page: AlbumPage),
          ],
        ),
        AutoRoute(
          path: 'albums',
          name: 'AlbumsPageRouter',
          page: EmptyRouterPage,
          children: [
            AutoRoute(path: '', page: RootAlbumsPage),
            AutoRoute(page: AlbumPage),
          ],
        ),
        AutoRoute(
          path: 'posts',
          name: 'PostsPageRouter',
          page: EmptyRouterPage,
          children: [
            AutoRoute(path: '', page: RootPostsPage),
            AutoRoute(page: PostPage),
          ],
        ),
      ],
    ),
  ],
)
class $AppRouter {}
