import 'package:flutter/material.dart';

ThemeData get light {
  return ThemeData.light().copyWith(
    colorScheme: ColorScheme.fromSeed(seedColor: Colors.tealAccent),
  );
}

ThemeData get dark {
  return ThemeData.dark().copyWith(
    colorScheme: ColorScheme.fromSeed(
      seedColor: Colors.brown,
      brightness: Brightness.dark,
    ),
  );
}
