import 'package:csapp/src/core/utils/constants.dart';
import 'package:csapp/src/domain/converters/map_converter.dart';
import 'package:csapp/src/domain/entities/album.dart';
import 'package:csapp/src/domain/entities/comment.dart';
import 'package:injectable/injectable.dart';
import 'package:shared_preferences/shared_preferences.dart';

@lazySingleton
class AppPrefs {
  final SharedPreferences _prefs;
  final MapConverter _mapConverter;

  AppPrefs(this._prefs) : _mapConverter = const MapConverter();

  Future<List<Album>> getAlbumList([List<int> userIdList = const []]) async {
    final stringList = _prefs.getStringList(StorageKeys.albums) ?? [];
    if (stringList.isEmpty) return [];

    final albums = stringList.map(
      (e) => Album.fromJson(_mapConverter.stringToMap(e)),
    );
    if (userIdList.isNotEmpty) {
      return albums.where((e) => userIdList.contains(e.userId)).toList();
    }
    return albums.toList();
  }

  Future<bool> saveAlbums(Iterable<Album> albums) {
    return _prefs.setStringList(
      StorageKeys.albums,
      albums.map((e) => _mapConverter.mapToString(e.toJson())).toList(),
    );
  }

  Future<List<Comment>> getComments([List<int> postIdList = const []]) async {
    final stringList = _prefs.getStringList(StorageKeys.comments) ?? [];
    if (stringList.isEmpty) return [];

    final comments = stringList.map(
      (e) => Comment.fromJson(_mapConverter.stringToMap(e)),
    );
    if (postIdList.isNotEmpty) {
      return comments.where((e) => postIdList.contains(e.postId)).toList();
    }
    return comments.toList();
  }

  Future<bool> saveComments(Iterable<Comment> comments) async {
    final readComments = (await getComments())..addAll(comments);
    return _prefs.setStringList(
      StorageKeys.comments,
      readComments.map((e) => _mapConverter.mapToString(e.toJson())).toList(),
    );
  }
}
