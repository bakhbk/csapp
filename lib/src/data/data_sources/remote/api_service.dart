import 'package:csapp/src/core/utils/constants.dart';
import 'package:csapp/src/domain/entities/album.dart';
import 'package:csapp/src/domain/entities/comment.dart';
import 'package:csapp/src/domain/entities/photo.dart';
import 'package:csapp/src/domain/entities/post.dart';
import 'package:csapp/src/domain/entities/user.dart';
import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:retrofit/retrofit.dart';

part 'api_service.g.dart';

@RestApi(baseUrl: ApiKeys.baseUrl)
abstract class ApiService {
  @singleton
  factory ApiService(Dio dio, {String baseUrl}) = _ApiService;

  @GET(ApiKeys.usersEndPoint)
  Future<HttpResponse<List<User>>> getUsers();

  @GET('${ApiKeys.usersEndPoint}/{id}')
  Future<HttpResponse<User>> getUserById(@Path("id") int userId);

  @GET(ApiKeys.postsEndPoint)
  Future<HttpResponse<List<Post>>> getPostList();

  @GET('${ApiKeys.usersEndPoint}/{id}${ApiKeys.postsEndPoint}')
  Future<HttpResponse<List<Post>>> getPostsByUserId(@Path("id") int userId);

  @GET('${ApiKeys.usersEndPoint}/{id}${ApiKeys.albumsEndPoint}')
  Future<HttpResponse<List<Album>>> getAlbumsByUserId(@Path("id") int userId);

  @GET(ApiKeys.albumsEndPoint)
  Future<HttpResponse<List<Album>>> getAlbumList();

  @GET('${ApiKeys.albumsEndPoint}/{id}${ApiKeys.photosEndPoint}')
  Future<HttpResponse<List<Photo>>> getPhotoByAlbumId(@Path("id") int albumId);

  @GET('${ApiKeys.postsEndPoint}/{id}${ApiKeys.commentsEndPoint}')
  Future<HttpResponse<List<Comment>>> getCommentsByPostId(
    @Path("id") int postId,
  );
}
