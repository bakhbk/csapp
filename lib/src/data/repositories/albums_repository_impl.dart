import 'package:csapp/src/core/params/request_params.dart';
import 'package:csapp/src/core/resources/data_state.dart';
import 'package:csapp/src/core/utils/api.dart';
import 'package:csapp/src/data/data_sources/local/app_prefs.dart';
import 'package:csapp/src/data/data_sources/remote/api_service.dart';
import 'package:csapp/src/domain/entities/album.dart';
import 'package:csapp/src/domain/entities/photo.dart';
import 'package:csapp/src/domain/repositories/album_repository.dart';
import 'package:injectable/injectable.dart';

@lazySingleton
class AlbumsRepositoryImpl implements AlbumsRepository {
  final ApiService _newsApiService;
  final AppPrefs _appPrefs;

  const AlbumsRepositoryImpl(this._newsApiService, this._appPrefs);

  @override
  Future<DataState<List<Album>>> getAlbumList() async {
    final cachedAlbums = await _appPrefs.getAlbumList();
    if (cachedAlbums.isNotEmpty) return DataSuccess(cachedAlbums);

    DataState<List<Album>> dataState = await throwableResponseToDataState(
      () => _newsApiService.getAlbumList(),
    );

    if (dataState is DataSuccess<List<Album>>) {
      dataState = DataSuccess(await _loadPhotoListToAlbumList(dataState.data));

      await _appPrefs.saveAlbums(dataState.data);
    }

    return dataState;
  }

  @override
  Future<DataState<List<Album>>> getAlbumsByUserId(
    AlbumsRequestParams params,
  ) async {
    final DataState<List<Album>> state = await throwableResponseToDataState(
      () => _newsApiService.getAlbumsByUserId(params.userId),
    );
    if (state is DataSuccess<List<Album>>) {
      final _albums = params.takeEntireList
          ? state.data
          : state.data.take(params.takeCount);

      return DataSuccess(await _loadPhotoListToAlbumList(_albums));
    }

    return state;
  }

  Future<List<Album>> _loadPhotoListToAlbumList(Iterable<Album> _albums) {
    return Future.wait(_albums.map(_loadPhotoList));
  }

  Future<Album> _loadPhotoList(Album album) async {
    final DataState<List<Photo>> state = await throwableResponseToDataState(
      () => _newsApiService.getPhotoByAlbumId(album.id),
    );
    return album.copyWith(photos: state.data);
  }
}
