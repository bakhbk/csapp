import 'package:csapp/src/core/resources/data_state.dart';
import 'package:csapp/src/core/utils/api.dart';
import 'package:csapp/src/data/data_sources/local/app_prefs.dart';
import 'package:csapp/src/data/data_sources/remote/api_service.dart';
import 'package:csapp/src/domain/entities/comment.dart';
import 'package:csapp/src/domain/entities/post.dart';
import 'package:csapp/src/domain/repositories/posts_repository.dart';
import 'package:injectable/injectable.dart';

@lazySingleton
class PostsRepositoryImpl implements PostsRepository {
  final ApiService _newsApiService;
  final AppPrefs _prefs;

  const PostsRepositoryImpl(this._newsApiService, this._prefs);

  @override
  Future<DataState<List<Post>>> getPosts() {
    return throwableResponseToDataState(() => _newsApiService.getPostList());
  }

  @override
  Future<DataState<List<Post>>> getPostsByUserId(int userId) {
    return throwableResponseToDataState(
      () => _newsApiService.getPostsByUserId(userId),
    );
  }

  @override
  Future<DataState<List<Comment>>> getCommentsByPostId(int postId) async {
    final DataState<List<Comment>> state = await throwableResponseToDataState(
      () => _newsApiService.getCommentsByPostId(postId),
    );
    return state..data.addAll(await _prefs.getComments([postId]));
  }

  @override
  Future<DataState<List<Comment>>> sendComment(Comment comment) async {
    await _prefs.saveComments([comment]);
    final DataState<List<Comment>> state = await throwableResponseToDataState(
      () => _newsApiService.getCommentsByPostId(comment.postId),
    );

    return state..data.addAll(await _prefs.getComments([comment.postId]));
  }
}
