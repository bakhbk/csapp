import 'package:csapp/src/core/resources/data_state.dart';
import 'package:csapp/src/core/utils/api.dart';
import 'package:csapp/src/data/data_sources/remote/api_service.dart';
import 'package:csapp/src/domain/entities/user.dart';
import 'package:csapp/src/domain/repositories/users_repository.dart';
import 'package:injectable/injectable.dart';

@lazySingleton
class UsersRepositoryImpl implements UsersRepository {
  final ApiService _newsApiService;

  const UsersRepositoryImpl(this._newsApiService);

  @override
  Future<DataState<List<User>>> getUsers() {
    return throwableResponseToDataState(
      () => _newsApiService.getUsers(),
    );
  }

  @override
  Future<DataState<User>> getUserById(int userId) {
    return throwableResponseToDataState(
      () => _newsApiService.getUserById(userId),
    );
  }
}
