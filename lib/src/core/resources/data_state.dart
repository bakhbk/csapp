import 'package:dio/dio.dart';

abstract class DataState<T> {
  final T data;
  final DioError? error;

  const DataState({T? data, this.error}) : data = data ?? const {} as T;
}

class DataSuccess<T> extends DataState<T> {
  const DataSuccess(T data) : super(data: data);
}

class DataFailed<T> extends DataState<T> implements Exception {
  const DataFailed([DioError? error]) : super(error: error);
}
