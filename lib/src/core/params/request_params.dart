import 'package:csapp/src/core/utils/constants.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'request_params.freezed.dart';

@freezed
class AlbumsRequestParams with _$AlbumsRequestParams {
  const factory AlbumsRequestParams({
    required int userId,
    @Default(kEntireList) int takeCount,
  }) = _Params;

  const AlbumsRequestParams._();

  bool get takeEntireList => takeCount == kEntireList;
}
