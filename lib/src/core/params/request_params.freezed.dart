// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'request_params.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$AlbumsRequestParamsTearOff {
  const _$AlbumsRequestParamsTearOff();

  _Params call({required int userId, int takeCount = kEntireList}) {
    return _Params(
      userId: userId,
      takeCount: takeCount,
    );
  }
}

/// @nodoc
const $AlbumsRequestParams = _$AlbumsRequestParamsTearOff();

/// @nodoc
mixin _$AlbumsRequestParams {
  int get userId => throw _privateConstructorUsedError;
  int get takeCount => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $AlbumsRequestParamsCopyWith<AlbumsRequestParams> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AlbumsRequestParamsCopyWith<$Res> {
  factory $AlbumsRequestParamsCopyWith(
          AlbumsRequestParams value, $Res Function(AlbumsRequestParams) then) =
      _$AlbumsRequestParamsCopyWithImpl<$Res>;
  $Res call({int userId, int takeCount});
}

/// @nodoc
class _$AlbumsRequestParamsCopyWithImpl<$Res>
    implements $AlbumsRequestParamsCopyWith<$Res> {
  _$AlbumsRequestParamsCopyWithImpl(this._value, this._then);

  final AlbumsRequestParams _value;
  // ignore: unused_field
  final $Res Function(AlbumsRequestParams) _then;

  @override
  $Res call({
    Object? userId = freezed,
    Object? takeCount = freezed,
  }) {
    return _then(_value.copyWith(
      userId: userId == freezed
          ? _value.userId
          : userId // ignore: cast_nullable_to_non_nullable
              as int,
      takeCount: takeCount == freezed
          ? _value.takeCount
          : takeCount // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc
abstract class _$ParamsCopyWith<$Res>
    implements $AlbumsRequestParamsCopyWith<$Res> {
  factory _$ParamsCopyWith(_Params value, $Res Function(_Params) then) =
      __$ParamsCopyWithImpl<$Res>;
  @override
  $Res call({int userId, int takeCount});
}

/// @nodoc
class __$ParamsCopyWithImpl<$Res>
    extends _$AlbumsRequestParamsCopyWithImpl<$Res>
    implements _$ParamsCopyWith<$Res> {
  __$ParamsCopyWithImpl(_Params _value, $Res Function(_Params) _then)
      : super(_value, (v) => _then(v as _Params));

  @override
  _Params get _value => super._value as _Params;

  @override
  $Res call({
    Object? userId = freezed,
    Object? takeCount = freezed,
  }) {
    return _then(_Params(
      userId: userId == freezed
          ? _value.userId
          : userId // ignore: cast_nullable_to_non_nullable
              as int,
      takeCount: takeCount == freezed
          ? _value.takeCount
          : takeCount // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$_Params extends _Params {
  const _$_Params({required this.userId, this.takeCount = kEntireList})
      : super._();

  @override
  final int userId;
  @JsonKey()
  @override
  final int takeCount;

  @override
  String toString() {
    return 'AlbumsRequestParams(userId: $userId, takeCount: $takeCount)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _Params &&
            const DeepCollectionEquality().equals(other.userId, userId) &&
            const DeepCollectionEquality().equals(other.takeCount, takeCount));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(userId),
      const DeepCollectionEquality().hash(takeCount));

  @JsonKey(ignore: true)
  @override
  _$ParamsCopyWith<_Params> get copyWith =>
      __$ParamsCopyWithImpl<_Params>(this, _$identity);
}

abstract class _Params extends AlbumsRequestParams {
  const factory _Params({required int userId, int takeCount}) = _$_Params;
  const _Params._() : super._();

  @override
  int get userId;
  @override
  int get takeCount;
  @override
  @JsonKey(ignore: true)
  _$ParamsCopyWith<_Params> get copyWith => throw _privateConstructorUsedError;
}
