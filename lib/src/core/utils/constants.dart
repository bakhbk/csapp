class ApiKeys {
  static const baseUrl = 'https://jsonplaceholder.typicode.com';
  static const usersEndPoint = '/users';
  static const postsEndPoint = '/posts';
  static const albumsEndPoint = '/albums';
  static const photosEndPoint = '/photos';
  static const commentsEndPoint = '/comments';
}

class StorageKeys {
  static const albums = 'albums';
  static const comments = 'comments';
}

const int kEmptyUserId = -1;
const int kEntireList = -1;
