import 'package:csapp/src/di/injector.dart';
import 'package:logger/logger.dart';

final _log = getIt<Logger>();

extension DynamicExt on dynamic {
  void logI(Object? msg) => _log.i('$runtimeType, $msg');

  void logE(Object? msg) => _log.e('$runtimeType, $msg');

  void logD(Object? msg) => _log.d('$runtimeType, $msg');

  void logV(Object? msg) => _log.v('$runtimeType, $msg');

  void logW(Object? msg) => _log.w('$runtimeType, $msg');

  void logWtf(Object? msg) => _log.wtf('$runtimeType, $msg');
}

void appErrors(Object e, StackTrace s) => _log.e('App', e, s);
