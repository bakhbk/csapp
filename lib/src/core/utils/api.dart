import 'dart:io';

import 'package:csapp/src/core/resources/data_state.dart';
import 'package:dio/dio.dart';
import 'package:retrofit/dio.dart';
import 'package:retrofit/retrofit.dart';

Future<DataState<R>> throwableResponseToDataState<T, R>(
  Future<HttpResponse<T>> Function() call,
) async {
  try {
    final httpResponse = await call();
    if (httpResponse.response.statusCode == HttpStatus.ok) {
      return DataSuccess(httpResponse.data as R);
    }
    throw DataFailed(_dioError(httpResponse));
  } on DioError catch (e) {
    throw DataFailed(e);
  }
}

DioError _dioError(HttpResponse httpResponse) {
  return DioError(
    error: httpResponse.response.statusMessage,
    response: httpResponse.response,
    type: DioErrorType.response,
    requestOptions: httpResponse.response.requestOptions,
  );
}
