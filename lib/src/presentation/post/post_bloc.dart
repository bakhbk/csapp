import 'package:csapp/src/core/resources/data_state.dart';
import 'package:csapp/src/domain/use_cases/get_user_by_id_use_case.dart';
import 'package:csapp/src/presentation/post/post_event.dart';
import 'package:csapp/src/presentation/post/post_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class PostBloc extends Bloc<PostEvent, PostState> {
  final GetUserByIdUseCase _getUserByIdUseCase;

  PostBloc(this._getUserByIdUseCase) : super(const PostState.loading()) {
    on<InitEvent>(_init);
  }

  Future<void> _init(InitEvent event, Emitter<PostState> emit) async {
    try {
      final dataState = await _getUserByIdUseCase.call(event.post.userId);
      emit(PostState.loaded(event.post, dataState.data));
    } on DataFailed {
      emit(const PostState.loadingFailed());
    }
  }
}
