import 'package:csapp/src/domain/entities/post.dart';
import 'package:csapp/src/domain/entities/user.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'post_state.freezed.dart';

@freezed
class PostState with _$PostState {
  const factory PostState.loading() = LoadingState;

  const factory PostState.loaded(Post post, User user) = LoadedState;

  const factory PostState.loadingFailed() = LoadingFailedState;
}
