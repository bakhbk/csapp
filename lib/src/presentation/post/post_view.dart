import 'package:csapp/src/core/utils/string.dart';
import 'package:csapp/src/di/injector.dart';
import 'package:csapp/src/domain/entities/post.dart';
import 'package:csapp/src/presentation/post/comments/comments_view.dart';
import 'package:csapp/src/presentation/post/post_bloc.dart';
import 'package:csapp/src/presentation/post/post_event.dart';
import 'package:csapp/src/presentation/post/post_state.dart';
import 'package:csapp/src/presentation/widgets/failed_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:skeletons/skeletons.dart';

class PostPage extends StatelessWidget {
  final Post post;

  const PostPage({required this.post, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(post.title.capitalize())),
      body: BlocProvider(
        create: (_) => getIt<PostBloc>()..add(PostEvent.init(post)),
        child: BlocBuilder<PostBloc, PostState>(
          builder: (_, state) => _buildBody(state),
        ),
      ),
    );
  }

  Widget _buildBody(PostState state) {
    if (state is LoadingState) {
      return Column(
        mainAxisSize: MainAxisSize.min,
        children: [SkeletonParagraph(), SkeletonParagraph()],
      );
    }
    if (state is LoadedState) return _buildLoadedBody(state);
    if (state is LoadingFailedState) return const FailedView();
    return const SizedBox();
  }

  Widget _buildLoadedBody(LoadedState state) {
    final user = state.user;
    final post = state.post;
    const divider = Divider(height: 12);
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          _buildRow('Author', user.name),
          divider,
          _buildRow('Email', user.email),
          divider,
          ..._buildInnerChildren('Post', post.title),
          divider,
          ..._buildInnerChildren('Body', post.body),
          divider,
          const Text('Comments'),
          CommentsPage(postId: post.id),
        ],
      ),
    );
  }

  Widget _buildRow(String title, String body) {
    return Row(children: [Text('$title: '), Text(body)]);
  }

  List<Widget> _buildInnerChildren(String title, String body) {
    return [Text('$title: '), Text(body)];
  }
}
