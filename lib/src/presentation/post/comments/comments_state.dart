import 'package:csapp/src/domain/entities/comment.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'comments_state.freezed.dart';

@freezed
class CommentsState with _$CommentsState {
  const factory CommentsState.loading() = LoadingState;

  const factory CommentsState.loaded(List<Comment> comments) = LoadedState;

  const factory CommentsState.loadingFailed() = LoadingFailed;
}
