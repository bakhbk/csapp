import 'package:csapp/src/di/injector.dart';
import 'package:csapp/src/domain/entities/comment.dart';
import 'package:csapp/src/presentation/editable_dialog/editable_dialog.dart';
import 'package:csapp/src/presentation/post/comments/comments_bloc.dart';
import 'package:csapp/src/presentation/post/comments/comments_event.dart';
import 'package:csapp/src/presentation/post/comments/comments_state.dart';
import 'package:csapp/src/presentation/widgets/failed_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:skeletons/skeletons.dart';

class CommentsPage extends StatelessWidget {
  final int postId;

  const CommentsPage({required this.postId, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => getIt<CommentsBloc>()..add(CommentsEvent.init(postId)),
      child: Builder(builder: _buildPage),
    );
  }

  Widget _buildPage(BuildContext context) {
    // final bloc = BlocProvider.of<CommentsBloc>(context);

    return BlocBuilder<CommentsBloc, CommentsState>(
      builder: (context, state) {
        if (state is LoadingState) return SkeletonParagraph();
        if (state is LoadedState) return _buildListView(state.comments);
        if (state is LoadingFailed) {
          return const SizedBox(height: 16, child: FailedView());
        }

        return const SizedBox();
      },
    );
  }

  Widget _buildListView(List<Comment> comments) {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 16),
        child: Column(
          children: [
            Expanded(
              child: ListView.separated(
                shrinkWrap: true,
                itemCount: comments.length,
                itemBuilder: (_, i) {
                  final comment = comments[i];
                  return Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Text('Name ${comment.name}'),
                      Text('Email ${comment.email}'),
                      const SizedBox(height: 4),
                      Flexible(child: Text(comment.body)),
                    ],
                  );
                },
                separatorBuilder: (_, __) => const Divider(),
              ),
            ),
            Builder(
              builder: (context) {
                final bloc = BlocProvider.of<CommentsBloc>(context);
                final colorScheme = Theme.of(context).colorScheme;
                const title = 'Add comment';
                return TextButton(
                  style: TextButton.styleFrom(
                    backgroundColor: colorScheme.secondaryContainer,
                  ),
                  onPressed: () => showEditableDialog(
                    title: title,
                    context: context,
                    onPressed: (map) => bloc.add(
                      CommentsEvent.sendComment(map, postId, comments),
                    ),
                  ),
                  child: const Text(title),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
