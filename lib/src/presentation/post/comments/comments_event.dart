import 'package:csapp/src/domain/entities/comment.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'comments_event.freezed.dart';

@freezed
class CommentsEvent with _$CommentsEvent {
  const factory CommentsEvent.init(int postId) = InitEvent;

  const factory CommentsEvent.sendComment(
    Map<String, dynamic> dataMap,
    int postId,
    List<Comment> comments,
  ) = SendCommentEvent;
}
