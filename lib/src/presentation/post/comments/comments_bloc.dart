import 'dart:math';

import 'package:csapp/src/core/resources/data_state.dart';
import 'package:csapp/src/core/utils/logger.dart';
import 'package:csapp/src/domain/entities/comment.dart';
import 'package:csapp/src/domain/use_cases/get_comments_by_post_id_use_case.dart';
import 'package:csapp/src/domain/use_cases/send_comment_use_case.dart';
import 'package:csapp/src/presentation/post/comments/comments_event.dart';
import 'package:csapp/src/presentation/post/comments/comments_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CommentsBloc extends Bloc<CommentsEvent, CommentsState> {
  final GetCommentsByPostIdUseCase _getCommentsByPostIdUseCase;
  final SendCommentUseCase _sendCommentUseCase;

  CommentsBloc(
    this._getCommentsByPostIdUseCase,
    this._sendCommentUseCase,
  ) : super(const CommentsState.loading()) {
    on<InitEvent>(_init);
    on<SendCommentEvent>(_sendComment);
  }

  Future<void> _init(InitEvent event, Emitter<CommentsState> emit) async {
    try {
      final dataState = await _getCommentsByPostIdUseCase.call(event.postId);
      emit(CommentsState.loaded(dataState.data));
    } on DataFailed catch (e) {
      logE(e);
      emit(const CommentsState.loadingFailed());
    }
  }

  Future<void> _sendComment(
    SendCommentEvent event,
    Emitter<CommentsState> emit,
  ) async {
    try {
      final json = event.dataMap
        ..addAll({
          'postId': event.postId,
          'id': event.comments.map((e) => e.id).fold(0, max) + 1,
        });

      final dataState = await _sendCommentUseCase.call(Comment.fromJson(json));
      emit(CommentsState.loaded(dataState.data));
    } on DataFailed catch (e) {
      logE(e);
      emit(const CommentsState.loadingFailed());
    }
  }
}
