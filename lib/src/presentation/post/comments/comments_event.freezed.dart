// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'comments_event.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$CommentsEventTearOff {
  const _$CommentsEventTearOff();

  InitEvent init(int postId) {
    return InitEvent(
      postId,
    );
  }

  SendCommentEvent sendComment(
      Map<String, dynamic> dataMap, int postId, List<Comment> comments) {
    return SendCommentEvent(
      dataMap,
      postId,
      comments,
    );
  }
}

/// @nodoc
const $CommentsEvent = _$CommentsEventTearOff();

/// @nodoc
mixin _$CommentsEvent {
  int get postId => throw _privateConstructorUsedError;

  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(int postId) init,
    required TResult Function(
            Map<String, dynamic> dataMap, int postId, List<Comment> comments)
        sendComment,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(int postId)? init,
    TResult Function(
            Map<String, dynamic> dataMap, int postId, List<Comment> comments)?
        sendComment,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(int postId)? init,
    TResult Function(
            Map<String, dynamic> dataMap, int postId, List<Comment> comments)?
        sendComment,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitEvent value) init,
    required TResult Function(SendCommentEvent value) sendComment,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitEvent value)? init,
    TResult Function(SendCommentEvent value)? sendComment,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitEvent value)? init,
    TResult Function(SendCommentEvent value)? sendComment,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $CommentsEventCopyWith<CommentsEvent> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CommentsEventCopyWith<$Res> {
  factory $CommentsEventCopyWith(
          CommentsEvent value, $Res Function(CommentsEvent) then) =
      _$CommentsEventCopyWithImpl<$Res>;
  $Res call({int postId});
}

/// @nodoc
class _$CommentsEventCopyWithImpl<$Res>
    implements $CommentsEventCopyWith<$Res> {
  _$CommentsEventCopyWithImpl(this._value, this._then);

  final CommentsEvent _value;
  // ignore: unused_field
  final $Res Function(CommentsEvent) _then;

  @override
  $Res call({
    Object? postId = freezed,
  }) {
    return _then(_value.copyWith(
      postId: postId == freezed
          ? _value.postId
          : postId // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc
abstract class $InitEventCopyWith<$Res>
    implements $CommentsEventCopyWith<$Res> {
  factory $InitEventCopyWith(InitEvent value, $Res Function(InitEvent) then) =
      _$InitEventCopyWithImpl<$Res>;
  @override
  $Res call({int postId});
}

/// @nodoc
class _$InitEventCopyWithImpl<$Res> extends _$CommentsEventCopyWithImpl<$Res>
    implements $InitEventCopyWith<$Res> {
  _$InitEventCopyWithImpl(InitEvent _value, $Res Function(InitEvent) _then)
      : super(_value, (v) => _then(v as InitEvent));

  @override
  InitEvent get _value => super._value as InitEvent;

  @override
  $Res call({
    Object? postId = freezed,
  }) {
    return _then(InitEvent(
      postId == freezed
          ? _value.postId
          : postId // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$InitEvent implements InitEvent {
  const _$InitEvent(this.postId);

  @override
  final int postId;

  @override
  String toString() {
    return 'CommentsEvent.init(postId: $postId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is InitEvent &&
            const DeepCollectionEquality().equals(other.postId, postId));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(postId));

  @JsonKey(ignore: true)
  @override
  $InitEventCopyWith<InitEvent> get copyWith =>
      _$InitEventCopyWithImpl<InitEvent>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(int postId) init,
    required TResult Function(
            Map<String, dynamic> dataMap, int postId, List<Comment> comments)
        sendComment,
  }) {
    return init(postId);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(int postId)? init,
    TResult Function(
            Map<String, dynamic> dataMap, int postId, List<Comment> comments)?
        sendComment,
  }) {
    return init?.call(postId);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(int postId)? init,
    TResult Function(
            Map<String, dynamic> dataMap, int postId, List<Comment> comments)?
        sendComment,
    required TResult orElse(),
  }) {
    if (init != null) {
      return init(postId);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitEvent value) init,
    required TResult Function(SendCommentEvent value) sendComment,
  }) {
    return init(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitEvent value)? init,
    TResult Function(SendCommentEvent value)? sendComment,
  }) {
    return init?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitEvent value)? init,
    TResult Function(SendCommentEvent value)? sendComment,
    required TResult orElse(),
  }) {
    if (init != null) {
      return init(this);
    }
    return orElse();
  }
}

abstract class InitEvent implements CommentsEvent {
  const factory InitEvent(int postId) = _$InitEvent;

  @override
  int get postId;
  @override
  @JsonKey(ignore: true)
  $InitEventCopyWith<InitEvent> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SendCommentEventCopyWith<$Res>
    implements $CommentsEventCopyWith<$Res> {
  factory $SendCommentEventCopyWith(
          SendCommentEvent value, $Res Function(SendCommentEvent) then) =
      _$SendCommentEventCopyWithImpl<$Res>;
  @override
  $Res call({Map<String, dynamic> dataMap, int postId, List<Comment> comments});
}

/// @nodoc
class _$SendCommentEventCopyWithImpl<$Res>
    extends _$CommentsEventCopyWithImpl<$Res>
    implements $SendCommentEventCopyWith<$Res> {
  _$SendCommentEventCopyWithImpl(
      SendCommentEvent _value, $Res Function(SendCommentEvent) _then)
      : super(_value, (v) => _then(v as SendCommentEvent));

  @override
  SendCommentEvent get _value => super._value as SendCommentEvent;

  @override
  $Res call({
    Object? dataMap = freezed,
    Object? postId = freezed,
    Object? comments = freezed,
  }) {
    return _then(SendCommentEvent(
      dataMap == freezed
          ? _value.dataMap
          : dataMap // ignore: cast_nullable_to_non_nullable
              as Map<String, dynamic>,
      postId == freezed
          ? _value.postId
          : postId // ignore: cast_nullable_to_non_nullable
              as int,
      comments == freezed
          ? _value.comments
          : comments // ignore: cast_nullable_to_non_nullable
              as List<Comment>,
    ));
  }
}

/// @nodoc

class _$SendCommentEvent implements SendCommentEvent {
  const _$SendCommentEvent(this.dataMap, this.postId, this.comments);

  @override
  final Map<String, dynamic> dataMap;
  @override
  final int postId;
  @override
  final List<Comment> comments;

  @override
  String toString() {
    return 'CommentsEvent.sendComment(dataMap: $dataMap, postId: $postId, comments: $comments)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is SendCommentEvent &&
            const DeepCollectionEquality().equals(other.dataMap, dataMap) &&
            const DeepCollectionEquality().equals(other.postId, postId) &&
            const DeepCollectionEquality().equals(other.comments, comments));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(dataMap),
      const DeepCollectionEquality().hash(postId),
      const DeepCollectionEquality().hash(comments));

  @JsonKey(ignore: true)
  @override
  $SendCommentEventCopyWith<SendCommentEvent> get copyWith =>
      _$SendCommentEventCopyWithImpl<SendCommentEvent>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(int postId) init,
    required TResult Function(
            Map<String, dynamic> dataMap, int postId, List<Comment> comments)
        sendComment,
  }) {
    return sendComment(dataMap, postId, comments);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(int postId)? init,
    TResult Function(
            Map<String, dynamic> dataMap, int postId, List<Comment> comments)?
        sendComment,
  }) {
    return sendComment?.call(dataMap, postId, comments);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(int postId)? init,
    TResult Function(
            Map<String, dynamic> dataMap, int postId, List<Comment> comments)?
        sendComment,
    required TResult orElse(),
  }) {
    if (sendComment != null) {
      return sendComment(dataMap, postId, comments);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitEvent value) init,
    required TResult Function(SendCommentEvent value) sendComment,
  }) {
    return sendComment(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitEvent value)? init,
    TResult Function(SendCommentEvent value)? sendComment,
  }) {
    return sendComment?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitEvent value)? init,
    TResult Function(SendCommentEvent value)? sendComment,
    required TResult orElse(),
  }) {
    if (sendComment != null) {
      return sendComment(this);
    }
    return orElse();
  }
}

abstract class SendCommentEvent implements CommentsEvent {
  const factory SendCommentEvent(
          Map<String, dynamic> dataMap, int postId, List<Comment> comments) =
      _$SendCommentEvent;

  Map<String, dynamic> get dataMap;
  @override
  int get postId;
  List<Comment> get comments;
  @override
  @JsonKey(ignore: true)
  $SendCommentEventCopyWith<SendCommentEvent> get copyWith =>
      throw _privateConstructorUsedError;
}
