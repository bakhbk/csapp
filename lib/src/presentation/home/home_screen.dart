import 'package:auto_route/auto_route.dart';
import 'package:csapp/src/config/routes/routes.gr.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AutoTabsRouter(
      routes: const [
        UsersPageRouter(),
        AlbumsPageRouter(),
        PostsPageRouter(),
      ],
      builder: (context, child, animation) {
        final tabsRouter = AutoTabsRouter.of(context);
        return Scaffold(
          body: FadeTransition(
            opacity: animation,
            child: child,
          ),
          bottomNavigationBar: BottomNavigationBar(
            currentIndex: tabsRouter.activeIndex,
            onTap: tabsRouter.setActiveIndex,
            items: const [
              BottomNavigationBarItem(label: 'Users', icon: Icon(Icons.people)),
              BottomNavigationBarItem(label: 'Albums', icon: Icon(Icons.album)),
              BottomNavigationBarItem(
                label: 'Posts',
                icon: Icon(Icons.newspaper),
              ),
            ],
          ),
        );
      },
    );
  }
}
