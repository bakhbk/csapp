import 'package:csapp/src/config/routes/routes.gr.dart';
import 'package:csapp/src/config/themes/themes.dart' as theme;
import 'package:csapp/src/di/injector.dart';
import 'package:flutter/material.dart';

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _router = getIt<AppRouter>();
    return MaterialApp.router(
      title: "Application",
      debugShowCheckedModeBanner: false,
      // themeMode: ThemeMode.dark,
      theme: theme.light,
      darkTheme: theme.dark,
      routerDelegate: _router.delegate(),
      routeInformationParser: _router.defaultRouteParser(),
    );
  }
}
