import 'dart:async';

import 'package:csapp/src/config/routes/routes.gr.dart';
import 'package:csapp/src/core/resources/data_state.dart';
import 'package:csapp/src/core/utils/logger.dart';
import 'package:csapp/src/domain/use_cases/get_users_use_case.dart';
import 'package:csapp/src/presentation/users/users_event.dart';
import 'package:csapp/src/presentation/users/users_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class UsersBloc extends Bloc<UsersEvent, UsersState> {
  final GetUsersUseCase _getUsersUseCase;
  final AppRouter _router;

  UsersBloc(
    this._getUsersUseCase,
    this._router,
  ) : super(const UsersState.loading()) {
    on<InitEvent>(_init);
    on<OpenUserByIdEvent>(_openUserById);
  }

  Future<void> _init(InitEvent event, Emitter<UsersState> emit) async {
    try {
      final dataState = await _getUsersUseCase.call();
      emit(UsersState.loaded(dataState.data));
    } on DataFailed catch (e) {
      logE(e.data);
      emit(const UsersState.loadingFailed());
    }
  }

  FutureOr<void> _openUserById(OpenUserByIdEvent event, _) async {
    await _router.push(UserPageRoute(userId: event.userId));
  }
}
