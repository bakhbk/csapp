import 'package:csapp/src/domain/entities/user.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'users_state.freezed.dart';

@freezed
class UsersState with _$UsersState {
  const factory UsersState.loading() = LoadingState;

  const factory UsersState.loaded(List<User> users) = LoadedState;

  const factory UsersState.loadingFailed() = LoadingFailedState;
}
