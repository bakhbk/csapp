import 'package:csapp/src/di/injector.dart';
import 'package:csapp/src/domain/entities/user.dart';
import 'package:csapp/src/presentation/users/users_bloc.dart';
import 'package:csapp/src/presentation/users/users_event.dart';
import 'package:csapp/src/presentation/users/users_state.dart';
import 'package:csapp/src/presentation/widgets/failed_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:skeletons/skeletons.dart';

part 'user_list_view.dart';

class UsersPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => getIt<UsersBloc>()..add(const UsersEvent.init()),
      child: Builder(builder: (context) => _buildPage(context)),
    );
  }

  Widget _buildPage(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Users')),
      body: SafeArea(
        child: BlocBuilder<UsersBloc, UsersState>(
          builder: (context, state) {
            if (state is LoadingState) return SkeletonListView();
            if (state is LoadedState) return _UserListView(users: state.users);
            if (state is LoadingFailedState) return const FailedView();
            return const SizedBox();
          },
        ),
      ),
    );
  }
}
