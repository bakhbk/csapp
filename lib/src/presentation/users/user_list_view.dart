part of 'users_view.dart';

class _UserListView extends StatelessWidget {
  final List<User> users;

  const _UserListView({required this.users, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _usersBloc = BlocProvider.of<UsersBloc>(context);
    return ListView.separated(
      itemCount: users.length,
      itemBuilder: (_, i) {
        final user = users[i];
        return ListTile(
          onTap: () => _usersBloc.add(UsersEvent.openByUserId(user.id)),
          title: Text(user.username),
          subtitle: Text(user.name),
        );
      },
      separatorBuilder: (_, __) => const Divider(),
    );
  }
}
