part of '../user_view.dart';

class _DetailsView extends StatelessWidget {
  final User user;

  const _DetailsView({required this.user, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final entries = user.toJson().entries.where((e) => e.key != 'id').toList();
    return ListView.separated(
      itemCount: entries.length,
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      itemBuilder: (_, i) {
        final entry = entries[i];
        final value = entry.value;
        if (value is Map) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Text(entry.key.capitalize()),
              const SizedBox(height: 4),
              for (var o in value.entries) _MapEntryView(o),
            ],
          );
        }
        return _MapEntryView(entry);
      },
      separatorBuilder: (_, __) => const Divider(),
    );
  }
}

class _MapEntryView extends StatelessWidget {
  final MapEntry<dynamic, dynamic> entry;

  const _MapEntryView(this.entry, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text('${entry.key.toString().capitalize()}: ${entry.value}');
  }
}
