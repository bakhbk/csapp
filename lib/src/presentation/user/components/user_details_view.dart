part of '../user_view.dart';

class _UserDetailsView extends StatelessWidget {
  final UserLoadedState state;

  const _UserDetailsView({required this.state, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(state.user.username)),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Flexible(child: _DetailsView(user: state.user)),
                Flexible(child: _PostPreview(userId: state.user.id)),
                Flexible(child: _AlbumPreview(userId: state.user.id)),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
