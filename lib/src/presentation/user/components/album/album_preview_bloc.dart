import 'dart:async';

import 'package:csapp/src/config/routes/routes.gr.dart';
import 'package:csapp/src/core/params/request_params.dart';
import 'package:csapp/src/core/resources/data_state.dart';
import 'package:csapp/src/domain/use_cases/get_albums_by_user_id_use_case.dart';
import 'package:csapp/src/presentation/user/components/album/album_preview_event.dart';
import 'package:csapp/src/presentation/user/components/album/album_preview_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class AlbumPreviewBloc extends Bloc<AlbumPreviewEvent, AlbumPreviewState> {
  final GetAlbumsByUserIdUseCase _getAlbumsByUserIdUseCase;
  final AppRouter _router;

  AlbumPreviewBloc(
    this._getAlbumsByUserIdUseCase,
    this._router,
  ) : super(const AlbumPreviewState.loading()) {
    on<InitEvent>(_init);
    on<OpenAlbumsEvent>(_openAlbumsView);
  }

  Future<void> _init(InitEvent event, Emitter<AlbumPreviewState> emit) async {
    try {
      final dataState = await _getAlbumsByUserIdUseCase.call(
        AlbumsRequestParams(userId: event.userId, takeCount: 3),
      );

      emit(AlbumPreviewState.loaded(dataState.data));
    } on DataFailed {
      emit(const AlbumPreviewState.loadingFailed());
    }
  }

  FutureOr<void> _openAlbumsView(OpenAlbumsEvent event, __) async {
    await _router.push(AlbumsPageRoute(userId: event.userId));
  }
}
