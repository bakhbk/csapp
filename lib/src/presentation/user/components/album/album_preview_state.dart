import 'package:csapp/src/domain/entities/album.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'album_preview_state.freezed.dart';

@freezed
class AlbumPreviewState with _$AlbumPreviewState {
  const factory AlbumPreviewState.loading() = LoadingState;

  const factory AlbumPreviewState.loaded(List<Album> albums) = LoadedState;

  const factory AlbumPreviewState.loadingFailed() = LoadingFailed;
}
