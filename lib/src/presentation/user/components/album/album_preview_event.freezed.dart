// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'album_preview_event.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$AlbumPreviewEventTearOff {
  const _$AlbumPreviewEventTearOff();

  InitEvent init(int userId) {
    return InitEvent(
      userId,
    );
  }

  OpenAlbumsEvent openAlbums(int userId) {
    return OpenAlbumsEvent(
      userId,
    );
  }
}

/// @nodoc
const $AlbumPreviewEvent = _$AlbumPreviewEventTearOff();

/// @nodoc
mixin _$AlbumPreviewEvent {
  int get userId => throw _privateConstructorUsedError;

  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(int userId) init,
    required TResult Function(int userId) openAlbums,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(int userId)? init,
    TResult Function(int userId)? openAlbums,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(int userId)? init,
    TResult Function(int userId)? openAlbums,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitEvent value) init,
    required TResult Function(OpenAlbumsEvent value) openAlbums,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitEvent value)? init,
    TResult Function(OpenAlbumsEvent value)? openAlbums,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitEvent value)? init,
    TResult Function(OpenAlbumsEvent value)? openAlbums,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $AlbumPreviewEventCopyWith<AlbumPreviewEvent> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AlbumPreviewEventCopyWith<$Res> {
  factory $AlbumPreviewEventCopyWith(
          AlbumPreviewEvent value, $Res Function(AlbumPreviewEvent) then) =
      _$AlbumPreviewEventCopyWithImpl<$Res>;
  $Res call({int userId});
}

/// @nodoc
class _$AlbumPreviewEventCopyWithImpl<$Res>
    implements $AlbumPreviewEventCopyWith<$Res> {
  _$AlbumPreviewEventCopyWithImpl(this._value, this._then);

  final AlbumPreviewEvent _value;
  // ignore: unused_field
  final $Res Function(AlbumPreviewEvent) _then;

  @override
  $Res call({
    Object? userId = freezed,
  }) {
    return _then(_value.copyWith(
      userId: userId == freezed
          ? _value.userId
          : userId // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc
abstract class $InitEventCopyWith<$Res>
    implements $AlbumPreviewEventCopyWith<$Res> {
  factory $InitEventCopyWith(InitEvent value, $Res Function(InitEvent) then) =
      _$InitEventCopyWithImpl<$Res>;
  @override
  $Res call({int userId});
}

/// @nodoc
class _$InitEventCopyWithImpl<$Res>
    extends _$AlbumPreviewEventCopyWithImpl<$Res>
    implements $InitEventCopyWith<$Res> {
  _$InitEventCopyWithImpl(InitEvent _value, $Res Function(InitEvent) _then)
      : super(_value, (v) => _then(v as InitEvent));

  @override
  InitEvent get _value => super._value as InitEvent;

  @override
  $Res call({
    Object? userId = freezed,
  }) {
    return _then(InitEvent(
      userId == freezed
          ? _value.userId
          : userId // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$InitEvent implements InitEvent {
  const _$InitEvent(this.userId);

  @override
  final int userId;

  @override
  String toString() {
    return 'AlbumPreviewEvent.init(userId: $userId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is InitEvent &&
            const DeepCollectionEquality().equals(other.userId, userId));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(userId));

  @JsonKey(ignore: true)
  @override
  $InitEventCopyWith<InitEvent> get copyWith =>
      _$InitEventCopyWithImpl<InitEvent>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(int userId) init,
    required TResult Function(int userId) openAlbums,
  }) {
    return init(userId);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(int userId)? init,
    TResult Function(int userId)? openAlbums,
  }) {
    return init?.call(userId);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(int userId)? init,
    TResult Function(int userId)? openAlbums,
    required TResult orElse(),
  }) {
    if (init != null) {
      return init(userId);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitEvent value) init,
    required TResult Function(OpenAlbumsEvent value) openAlbums,
  }) {
    return init(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitEvent value)? init,
    TResult Function(OpenAlbumsEvent value)? openAlbums,
  }) {
    return init?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitEvent value)? init,
    TResult Function(OpenAlbumsEvent value)? openAlbums,
    required TResult orElse(),
  }) {
    if (init != null) {
      return init(this);
    }
    return orElse();
  }
}

abstract class InitEvent implements AlbumPreviewEvent {
  const factory InitEvent(int userId) = _$InitEvent;

  @override
  int get userId;
  @override
  @JsonKey(ignore: true)
  $InitEventCopyWith<InitEvent> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $OpenAlbumsEventCopyWith<$Res>
    implements $AlbumPreviewEventCopyWith<$Res> {
  factory $OpenAlbumsEventCopyWith(
          OpenAlbumsEvent value, $Res Function(OpenAlbumsEvent) then) =
      _$OpenAlbumsEventCopyWithImpl<$Res>;
  @override
  $Res call({int userId});
}

/// @nodoc
class _$OpenAlbumsEventCopyWithImpl<$Res>
    extends _$AlbumPreviewEventCopyWithImpl<$Res>
    implements $OpenAlbumsEventCopyWith<$Res> {
  _$OpenAlbumsEventCopyWithImpl(
      OpenAlbumsEvent _value, $Res Function(OpenAlbumsEvent) _then)
      : super(_value, (v) => _then(v as OpenAlbumsEvent));

  @override
  OpenAlbumsEvent get _value => super._value as OpenAlbumsEvent;

  @override
  $Res call({
    Object? userId = freezed,
  }) {
    return _then(OpenAlbumsEvent(
      userId == freezed
          ? _value.userId
          : userId // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$OpenAlbumsEvent implements OpenAlbumsEvent {
  const _$OpenAlbumsEvent(this.userId);

  @override
  final int userId;

  @override
  String toString() {
    return 'AlbumPreviewEvent.openAlbums(userId: $userId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is OpenAlbumsEvent &&
            const DeepCollectionEquality().equals(other.userId, userId));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(userId));

  @JsonKey(ignore: true)
  @override
  $OpenAlbumsEventCopyWith<OpenAlbumsEvent> get copyWith =>
      _$OpenAlbumsEventCopyWithImpl<OpenAlbumsEvent>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(int userId) init,
    required TResult Function(int userId) openAlbums,
  }) {
    return openAlbums(userId);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(int userId)? init,
    TResult Function(int userId)? openAlbums,
  }) {
    return openAlbums?.call(userId);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(int userId)? init,
    TResult Function(int userId)? openAlbums,
    required TResult orElse(),
  }) {
    if (openAlbums != null) {
      return openAlbums(userId);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitEvent value) init,
    required TResult Function(OpenAlbumsEvent value) openAlbums,
  }) {
    return openAlbums(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitEvent value)? init,
    TResult Function(OpenAlbumsEvent value)? openAlbums,
  }) {
    return openAlbums?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitEvent value)? init,
    TResult Function(OpenAlbumsEvent value)? openAlbums,
    required TResult orElse(),
  }) {
    if (openAlbums != null) {
      return openAlbums(this);
    }
    return orElse();
  }
}

abstract class OpenAlbumsEvent implements AlbumPreviewEvent {
  const factory OpenAlbumsEvent(int userId) = _$OpenAlbumsEvent;

  @override
  int get userId;
  @override
  @JsonKey(ignore: true)
  $OpenAlbumsEventCopyWith<OpenAlbumsEvent> get copyWith =>
      throw _privateConstructorUsedError;
}
