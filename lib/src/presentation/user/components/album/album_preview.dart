part of '../../user_view.dart';

class _AlbumPreview extends StatelessWidget {
  final int userId;

  const _AlbumPreview({required this.userId, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) {
        return getIt<AlbumPreviewBloc>()..add(AlbumPreviewEvent.init(userId));
      },
      child: _buildPage(),
    );
  }

  Widget _buildPage() {
    return Material(
      child: InkWell(
        onTap: () => getIt<AlbumPreviewBloc>().add(
          AlbumPreviewEvent.openAlbums(userId),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            const Text('Albums:'),
            BlocBuilder<AlbumPreviewBloc, AlbumPreviewState>(
              builder: (context, state) {
                if (state is LoadingState) return const _SkeletonPreview();
                if (state is LoadedState) {
                  return AlbumListView(
                    albums: state.albums,
                    physics: const NeverScrollableScrollPhysics(),
                  );
                }
                if (state is LoadingFailed) {
                  return const SizedBox(height: 16, child: FailedView());
                }
                return const SizedBox();
              },
            ),
          ],
        ),
      ),
    );
  }
}
