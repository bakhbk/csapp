import 'package:freezed_annotation/freezed_annotation.dart';

part 'album_preview_event.freezed.dart';

@freezed
class AlbumPreviewEvent with _$AlbumPreviewEvent {
  const factory AlbumPreviewEvent.init(int userId) = InitEvent;
  const factory AlbumPreviewEvent.openAlbums(int userId) = OpenAlbumsEvent;
}
