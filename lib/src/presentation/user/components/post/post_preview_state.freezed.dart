// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'post_preview_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$PostPreviewStateTearOff {
  const _$PostPreviewStateTearOff();

  PostPreviewLoading loading() {
    return const PostPreviewLoading();
  }

  PostPreviewLoaded loaded(List<Post> posts) {
    return PostPreviewLoaded(
      posts,
    );
  }

  PostPreviewLoadingFailed loadingFailed() {
    return const PostPreviewLoadingFailed();
  }
}

/// @nodoc
const $PostPreviewState = _$PostPreviewStateTearOff();

/// @nodoc
mixin _$PostPreviewState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loading,
    required TResult Function(List<Post> posts) loaded,
    required TResult Function() loadingFailed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function(List<Post> posts)? loaded,
    TResult Function()? loadingFailed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function(List<Post> posts)? loaded,
    TResult Function()? loadingFailed,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(PostPreviewLoading value) loading,
    required TResult Function(PostPreviewLoaded value) loaded,
    required TResult Function(PostPreviewLoadingFailed value) loadingFailed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(PostPreviewLoading value)? loading,
    TResult Function(PostPreviewLoaded value)? loaded,
    TResult Function(PostPreviewLoadingFailed value)? loadingFailed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(PostPreviewLoading value)? loading,
    TResult Function(PostPreviewLoaded value)? loaded,
    TResult Function(PostPreviewLoadingFailed value)? loadingFailed,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PostPreviewStateCopyWith<$Res> {
  factory $PostPreviewStateCopyWith(
          PostPreviewState value, $Res Function(PostPreviewState) then) =
      _$PostPreviewStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$PostPreviewStateCopyWithImpl<$Res>
    implements $PostPreviewStateCopyWith<$Res> {
  _$PostPreviewStateCopyWithImpl(this._value, this._then);

  final PostPreviewState _value;
  // ignore: unused_field
  final $Res Function(PostPreviewState) _then;
}

/// @nodoc
abstract class $PostPreviewLoadingCopyWith<$Res> {
  factory $PostPreviewLoadingCopyWith(
          PostPreviewLoading value, $Res Function(PostPreviewLoading) then) =
      _$PostPreviewLoadingCopyWithImpl<$Res>;
}

/// @nodoc
class _$PostPreviewLoadingCopyWithImpl<$Res>
    extends _$PostPreviewStateCopyWithImpl<$Res>
    implements $PostPreviewLoadingCopyWith<$Res> {
  _$PostPreviewLoadingCopyWithImpl(
      PostPreviewLoading _value, $Res Function(PostPreviewLoading) _then)
      : super(_value, (v) => _then(v as PostPreviewLoading));

  @override
  PostPreviewLoading get _value => super._value as PostPreviewLoading;
}

/// @nodoc

class _$PostPreviewLoading implements PostPreviewLoading {
  const _$PostPreviewLoading();

  @override
  String toString() {
    return 'PostPreviewState.loading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is PostPreviewLoading);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loading,
    required TResult Function(List<Post> posts) loaded,
    required TResult Function() loadingFailed,
  }) {
    return loading();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function(List<Post> posts)? loaded,
    TResult Function()? loadingFailed,
  }) {
    return loading?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function(List<Post> posts)? loaded,
    TResult Function()? loadingFailed,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(PostPreviewLoading value) loading,
    required TResult Function(PostPreviewLoaded value) loaded,
    required TResult Function(PostPreviewLoadingFailed value) loadingFailed,
  }) {
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(PostPreviewLoading value)? loading,
    TResult Function(PostPreviewLoaded value)? loaded,
    TResult Function(PostPreviewLoadingFailed value)? loadingFailed,
  }) {
    return loading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(PostPreviewLoading value)? loading,
    TResult Function(PostPreviewLoaded value)? loaded,
    TResult Function(PostPreviewLoadingFailed value)? loadingFailed,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class PostPreviewLoading implements PostPreviewState {
  const factory PostPreviewLoading() = _$PostPreviewLoading;
}

/// @nodoc
abstract class $PostPreviewLoadedCopyWith<$Res> {
  factory $PostPreviewLoadedCopyWith(
          PostPreviewLoaded value, $Res Function(PostPreviewLoaded) then) =
      _$PostPreviewLoadedCopyWithImpl<$Res>;
  $Res call({List<Post> posts});
}

/// @nodoc
class _$PostPreviewLoadedCopyWithImpl<$Res>
    extends _$PostPreviewStateCopyWithImpl<$Res>
    implements $PostPreviewLoadedCopyWith<$Res> {
  _$PostPreviewLoadedCopyWithImpl(
      PostPreviewLoaded _value, $Res Function(PostPreviewLoaded) _then)
      : super(_value, (v) => _then(v as PostPreviewLoaded));

  @override
  PostPreviewLoaded get _value => super._value as PostPreviewLoaded;

  @override
  $Res call({
    Object? posts = freezed,
  }) {
    return _then(PostPreviewLoaded(
      posts == freezed
          ? _value.posts
          : posts // ignore: cast_nullable_to_non_nullable
              as List<Post>,
    ));
  }
}

/// @nodoc

class _$PostPreviewLoaded implements PostPreviewLoaded {
  const _$PostPreviewLoaded(this.posts);

  @override
  final List<Post> posts;

  @override
  String toString() {
    return 'PostPreviewState.loaded(posts: $posts)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is PostPreviewLoaded &&
            const DeepCollectionEquality().equals(other.posts, posts));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(posts));

  @JsonKey(ignore: true)
  @override
  $PostPreviewLoadedCopyWith<PostPreviewLoaded> get copyWith =>
      _$PostPreviewLoadedCopyWithImpl<PostPreviewLoaded>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loading,
    required TResult Function(List<Post> posts) loaded,
    required TResult Function() loadingFailed,
  }) {
    return loaded(posts);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function(List<Post> posts)? loaded,
    TResult Function()? loadingFailed,
  }) {
    return loaded?.call(posts);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function(List<Post> posts)? loaded,
    TResult Function()? loadingFailed,
    required TResult orElse(),
  }) {
    if (loaded != null) {
      return loaded(posts);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(PostPreviewLoading value) loading,
    required TResult Function(PostPreviewLoaded value) loaded,
    required TResult Function(PostPreviewLoadingFailed value) loadingFailed,
  }) {
    return loaded(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(PostPreviewLoading value)? loading,
    TResult Function(PostPreviewLoaded value)? loaded,
    TResult Function(PostPreviewLoadingFailed value)? loadingFailed,
  }) {
    return loaded?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(PostPreviewLoading value)? loading,
    TResult Function(PostPreviewLoaded value)? loaded,
    TResult Function(PostPreviewLoadingFailed value)? loadingFailed,
    required TResult orElse(),
  }) {
    if (loaded != null) {
      return loaded(this);
    }
    return orElse();
  }
}

abstract class PostPreviewLoaded implements PostPreviewState {
  const factory PostPreviewLoaded(List<Post> posts) = _$PostPreviewLoaded;

  List<Post> get posts;
  @JsonKey(ignore: true)
  $PostPreviewLoadedCopyWith<PostPreviewLoaded> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PostPreviewLoadingFailedCopyWith<$Res> {
  factory $PostPreviewLoadingFailedCopyWith(PostPreviewLoadingFailed value,
          $Res Function(PostPreviewLoadingFailed) then) =
      _$PostPreviewLoadingFailedCopyWithImpl<$Res>;
}

/// @nodoc
class _$PostPreviewLoadingFailedCopyWithImpl<$Res>
    extends _$PostPreviewStateCopyWithImpl<$Res>
    implements $PostPreviewLoadingFailedCopyWith<$Res> {
  _$PostPreviewLoadingFailedCopyWithImpl(PostPreviewLoadingFailed _value,
      $Res Function(PostPreviewLoadingFailed) _then)
      : super(_value, (v) => _then(v as PostPreviewLoadingFailed));

  @override
  PostPreviewLoadingFailed get _value =>
      super._value as PostPreviewLoadingFailed;
}

/// @nodoc

class _$PostPreviewLoadingFailed implements PostPreviewLoadingFailed {
  const _$PostPreviewLoadingFailed();

  @override
  String toString() {
    return 'PostPreviewState.loadingFailed()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is PostPreviewLoadingFailed);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loading,
    required TResult Function(List<Post> posts) loaded,
    required TResult Function() loadingFailed,
  }) {
    return loadingFailed();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function(List<Post> posts)? loaded,
    TResult Function()? loadingFailed,
  }) {
    return loadingFailed?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function(List<Post> posts)? loaded,
    TResult Function()? loadingFailed,
    required TResult orElse(),
  }) {
    if (loadingFailed != null) {
      return loadingFailed();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(PostPreviewLoading value) loading,
    required TResult Function(PostPreviewLoaded value) loaded,
    required TResult Function(PostPreviewLoadingFailed value) loadingFailed,
  }) {
    return loadingFailed(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(PostPreviewLoading value)? loading,
    TResult Function(PostPreviewLoaded value)? loaded,
    TResult Function(PostPreviewLoadingFailed value)? loadingFailed,
  }) {
    return loadingFailed?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(PostPreviewLoading value)? loading,
    TResult Function(PostPreviewLoaded value)? loaded,
    TResult Function(PostPreviewLoadingFailed value)? loadingFailed,
    required TResult orElse(),
  }) {
    if (loadingFailed != null) {
      return loadingFailed(this);
    }
    return orElse();
  }
}

abstract class PostPreviewLoadingFailed implements PostPreviewState {
  const factory PostPreviewLoadingFailed() = _$PostPreviewLoadingFailed;
}
