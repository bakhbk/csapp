part of '../../user_view.dart';

class _PostPreview extends StatelessWidget {
  final int userId;

  const _PostPreview({required this.userId, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) =>
          getIt<PostPreviewBloc>()..add(PostPreviewEvent.init(userId)),
      child: _buildPage(),
    );
  }

  Widget _buildPage() {
    // final bloc = BlocProvider.of<PostPreviewBloc>(context);

    return Material(
      child: InkWell(
        onTap: () => getIt<PostPreviewBloc>().add(
          PostPreviewEvent.openPostsByUserId(userId),
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 8),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              const Divider(),
              const Text('Posts:'),
              Flexible(
                child: BlocBuilder<PostPreviewBloc, PostPreviewState>(
                  builder: (context, state) {
                    if (state is PostPreviewLoading) {
                      return const _SkeletonPreview();
                    }

                    if (state is PostPreviewLoaded) {
                      return PostListView(
                        posts: state.posts,
                        physics: const NeverScrollableScrollPhysics(),
                      );
                    }
                    if (state is PostPreviewLoadingFailed) {
                      return const SizedBox(height: 16, child: FailedView());
                    }
                    return const SizedBox();
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
