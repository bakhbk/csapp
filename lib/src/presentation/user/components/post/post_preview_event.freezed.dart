// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'post_preview_event.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$PostPreviewEventTearOff {
  const _$PostPreviewEventTearOff();

  InitEvent init(int userId) {
    return InitEvent(
      userId,
    );
  }

  OpenPostEvent openPostsByUserId(int userId) {
    return OpenPostEvent(
      userId,
    );
  }
}

/// @nodoc
const $PostPreviewEvent = _$PostPreviewEventTearOff();

/// @nodoc
mixin _$PostPreviewEvent {
  int get userId => throw _privateConstructorUsedError;

  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(int userId) init,
    required TResult Function(int userId) openPostsByUserId,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(int userId)? init,
    TResult Function(int userId)? openPostsByUserId,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(int userId)? init,
    TResult Function(int userId)? openPostsByUserId,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitEvent value) init,
    required TResult Function(OpenPostEvent value) openPostsByUserId,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitEvent value)? init,
    TResult Function(OpenPostEvent value)? openPostsByUserId,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitEvent value)? init,
    TResult Function(OpenPostEvent value)? openPostsByUserId,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $PostPreviewEventCopyWith<PostPreviewEvent> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PostPreviewEventCopyWith<$Res> {
  factory $PostPreviewEventCopyWith(
          PostPreviewEvent value, $Res Function(PostPreviewEvent) then) =
      _$PostPreviewEventCopyWithImpl<$Res>;
  $Res call({int userId});
}

/// @nodoc
class _$PostPreviewEventCopyWithImpl<$Res>
    implements $PostPreviewEventCopyWith<$Res> {
  _$PostPreviewEventCopyWithImpl(this._value, this._then);

  final PostPreviewEvent _value;
  // ignore: unused_field
  final $Res Function(PostPreviewEvent) _then;

  @override
  $Res call({
    Object? userId = freezed,
  }) {
    return _then(_value.copyWith(
      userId: userId == freezed
          ? _value.userId
          : userId // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc
abstract class $InitEventCopyWith<$Res>
    implements $PostPreviewEventCopyWith<$Res> {
  factory $InitEventCopyWith(InitEvent value, $Res Function(InitEvent) then) =
      _$InitEventCopyWithImpl<$Res>;
  @override
  $Res call({int userId});
}

/// @nodoc
class _$InitEventCopyWithImpl<$Res> extends _$PostPreviewEventCopyWithImpl<$Res>
    implements $InitEventCopyWith<$Res> {
  _$InitEventCopyWithImpl(InitEvent _value, $Res Function(InitEvent) _then)
      : super(_value, (v) => _then(v as InitEvent));

  @override
  InitEvent get _value => super._value as InitEvent;

  @override
  $Res call({
    Object? userId = freezed,
  }) {
    return _then(InitEvent(
      userId == freezed
          ? _value.userId
          : userId // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$InitEvent implements InitEvent {
  const _$InitEvent(this.userId);

  @override
  final int userId;

  @override
  String toString() {
    return 'PostPreviewEvent.init(userId: $userId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is InitEvent &&
            const DeepCollectionEquality().equals(other.userId, userId));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(userId));

  @JsonKey(ignore: true)
  @override
  $InitEventCopyWith<InitEvent> get copyWith =>
      _$InitEventCopyWithImpl<InitEvent>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(int userId) init,
    required TResult Function(int userId) openPostsByUserId,
  }) {
    return init(userId);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(int userId)? init,
    TResult Function(int userId)? openPostsByUserId,
  }) {
    return init?.call(userId);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(int userId)? init,
    TResult Function(int userId)? openPostsByUserId,
    required TResult orElse(),
  }) {
    if (init != null) {
      return init(userId);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitEvent value) init,
    required TResult Function(OpenPostEvent value) openPostsByUserId,
  }) {
    return init(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitEvent value)? init,
    TResult Function(OpenPostEvent value)? openPostsByUserId,
  }) {
    return init?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitEvent value)? init,
    TResult Function(OpenPostEvent value)? openPostsByUserId,
    required TResult orElse(),
  }) {
    if (init != null) {
      return init(this);
    }
    return orElse();
  }
}

abstract class InitEvent implements PostPreviewEvent {
  const factory InitEvent(int userId) = _$InitEvent;

  @override
  int get userId;
  @override
  @JsonKey(ignore: true)
  $InitEventCopyWith<InitEvent> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $OpenPostEventCopyWith<$Res>
    implements $PostPreviewEventCopyWith<$Res> {
  factory $OpenPostEventCopyWith(
          OpenPostEvent value, $Res Function(OpenPostEvent) then) =
      _$OpenPostEventCopyWithImpl<$Res>;
  @override
  $Res call({int userId});
}

/// @nodoc
class _$OpenPostEventCopyWithImpl<$Res>
    extends _$PostPreviewEventCopyWithImpl<$Res>
    implements $OpenPostEventCopyWith<$Res> {
  _$OpenPostEventCopyWithImpl(
      OpenPostEvent _value, $Res Function(OpenPostEvent) _then)
      : super(_value, (v) => _then(v as OpenPostEvent));

  @override
  OpenPostEvent get _value => super._value as OpenPostEvent;

  @override
  $Res call({
    Object? userId = freezed,
  }) {
    return _then(OpenPostEvent(
      userId == freezed
          ? _value.userId
          : userId // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$OpenPostEvent implements OpenPostEvent {
  const _$OpenPostEvent(this.userId);

  @override
  final int userId;

  @override
  String toString() {
    return 'PostPreviewEvent.openPostsByUserId(userId: $userId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is OpenPostEvent &&
            const DeepCollectionEquality().equals(other.userId, userId));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(userId));

  @JsonKey(ignore: true)
  @override
  $OpenPostEventCopyWith<OpenPostEvent> get copyWith =>
      _$OpenPostEventCopyWithImpl<OpenPostEvent>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(int userId) init,
    required TResult Function(int userId) openPostsByUserId,
  }) {
    return openPostsByUserId(userId);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(int userId)? init,
    TResult Function(int userId)? openPostsByUserId,
  }) {
    return openPostsByUserId?.call(userId);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(int userId)? init,
    TResult Function(int userId)? openPostsByUserId,
    required TResult orElse(),
  }) {
    if (openPostsByUserId != null) {
      return openPostsByUserId(userId);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitEvent value) init,
    required TResult Function(OpenPostEvent value) openPostsByUserId,
  }) {
    return openPostsByUserId(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitEvent value)? init,
    TResult Function(OpenPostEvent value)? openPostsByUserId,
  }) {
    return openPostsByUserId?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitEvent value)? init,
    TResult Function(OpenPostEvent value)? openPostsByUserId,
    required TResult orElse(),
  }) {
    if (openPostsByUserId != null) {
      return openPostsByUserId(this);
    }
    return orElse();
  }
}

abstract class OpenPostEvent implements PostPreviewEvent {
  const factory OpenPostEvent(int userId) = _$OpenPostEvent;

  @override
  int get userId;
  @override
  @JsonKey(ignore: true)
  $OpenPostEventCopyWith<OpenPostEvent> get copyWith =>
      throw _privateConstructorUsedError;
}
