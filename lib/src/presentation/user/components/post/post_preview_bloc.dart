import 'dart:async';

import 'package:csapp/src/config/routes/routes.gr.dart';
import 'package:csapp/src/core/resources/data_state.dart';
import 'package:csapp/src/domain/use_cases/get_post_list_by_user_id_use_case.dart';
import 'package:csapp/src/presentation/user/components/post/post_preview_event.dart';
import 'package:csapp/src/presentation/user/components/post/post_preview_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class PostPreviewBloc extends Bloc<PostPreviewEvent, PostPreviewState> {
  final GetPostsByUserIdUseCase _getPostsByUserIdUseCase;
  final AppRouter _router;

  PostPreviewBloc(
    this._getPostsByUserIdUseCase,
    this._router,
  ) : super(const PostPreviewState.loading()) {
    on<InitEvent>(_init);
    on<OpenPostEvent>(_openPostView);
  }

  Future<void> _init(InitEvent event, Emitter<PostPreviewState> emit) async {
    try {
      final dataState = await _getPostsByUserIdUseCase.call(event.userId);
      final posts = dataState.data.take(3).toList();
      emit(PostPreviewState.loaded(posts));
    } on DataFailed {
      emit(const PostPreviewState.loadingFailed());
    }
  }

  FutureOr<void> _openPostView(OpenPostEvent event, _) {
    _router.push(PostsPageRoute(userId: event.userId));
  }
}
