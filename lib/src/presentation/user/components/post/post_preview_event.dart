import 'package:freezed_annotation/freezed_annotation.dart';

part 'post_preview_event.freezed.dart';

@freezed
class PostPreviewEvent with _$PostPreviewEvent {
  const factory PostPreviewEvent.init(int userId) = InitEvent;

  const factory PostPreviewEvent.openPostsByUserId(int userId) = OpenPostEvent;
}
