import 'package:csapp/src/domain/entities/post.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'post_preview_state.freezed.dart';

@freezed
class PostPreviewState with _$PostPreviewState {
  const factory PostPreviewState.loading() = PostPreviewLoading;

  const factory PostPreviewState.loaded(List<Post> posts) = PostPreviewLoaded;

  const factory PostPreviewState.loadingFailed() = PostPreviewLoadingFailed;
}
