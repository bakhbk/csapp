part of '../user_view.dart';

class _SkeletonPreview extends StatelessWidget {
  const _SkeletonPreview({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: const [
        SkeletonLine(),
        Divider(),
        SkeletonLine(),
        Divider(),
        SkeletonLine(),
      ],
    );
  }
}
