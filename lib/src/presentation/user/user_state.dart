import 'package:csapp/src/domain/entities/user.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'user_state.freezed.dart';

@freezed
class UserState with _$UserState {
  const factory UserState.loading() = UserLoadingState;

  const factory UserState.loaded(User user) = UserLoadedState;

  const factory UserState.loadingFailed() = UserLoadingFailedState;
}
