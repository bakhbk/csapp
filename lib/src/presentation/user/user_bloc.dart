import 'dart:async';

import 'package:csapp/src/core/resources/data_state.dart';
import 'package:csapp/src/domain/use_cases/get_user_by_id_use_case.dart';
import 'package:csapp/src/presentation/user/user_event.dart';
import 'package:csapp/src/presentation/user/user_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class UserBloc extends Bloc<UserEvent, UserState> {
  final GetUserByIdUseCase _getUserByIdUseCase;

  UserBloc(this._getUserByIdUseCase) : super(const UserState.loading()) {
    on<InitEvent>(_init);
  }

  FutureOr<void> _init(InitEvent event, emit) async {
    try {
      final dataState = await _getUserByIdUseCase.call(event.userId);
      emit(UserState.loaded(dataState.data));
    } on DataFailed {
      emit(const UserState.loadingFailed());
    }
  }
}
