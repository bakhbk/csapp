import 'package:csapp/src/core/utils/string.dart';
import 'package:csapp/src/di/injector.dart';
import 'package:csapp/src/domain/entities/user.dart';
import 'package:csapp/src/presentation/user/components/album/album_preview_bloc.dart';
import 'package:csapp/src/presentation/user/components/album/album_preview_event.dart';
import 'package:csapp/src/presentation/user/components/album/album_preview_state.dart';
import 'package:csapp/src/presentation/user/components/post/post_preview_bloc.dart';
import 'package:csapp/src/presentation/user/components/post/post_preview_event.dart';
import 'package:csapp/src/presentation/user/components/post/post_preview_state.dart';
import 'package:csapp/src/presentation/user/user_bloc.dart';
import 'package:csapp/src/presentation/user/user_event.dart';
import 'package:csapp/src/presentation/user/user_state.dart';
import 'package:csapp/src/presentation/widgets/album_list_view.dart';
import 'package:csapp/src/presentation/widgets/custom_skeleton.dart';
import 'package:csapp/src/presentation/widgets/failed_view.dart';
import 'package:csapp/src/presentation/widgets/post_list_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:skeletons/skeletons.dart';

part 'components/album/album_preview.dart';

part 'components/details_view.dart';

part 'components/post/post_preview_view.dart';

part 'components/skeleton_preview.dart';

part 'components/user_details_view.dart';

class UserPage extends StatelessWidget {
  final int userId;

  const UserPage({required this.userId, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => getIt<UserBloc>()..add(UserEvent.init(userId)),
      child: _buildPage(),
    );
  }

  Widget _buildPage() {
    return BlocBuilder<UserBloc, UserState>(
      builder: (_, state) {
        if (state is UserLoadingState) return const CSkeleton();
        if (state is UserLoadedState) return _UserDetailsView(state: state);
        if (state is UserLoadingFailedState) {
          return Scaffold(appBar: AppBar(), body: const FailedView());
        }
        return const SizedBox();
      },
    );
  }
}
