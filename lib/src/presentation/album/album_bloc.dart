import 'package:csapp/src/presentation/album/album_event.dart';
import 'package:csapp/src/presentation/album/album_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class AlbumBloc extends Bloc<AlbumEvent, AlbumState> {
  AlbumBloc() : super(const AlbumState.loading()) {
    on<InitEvent>(_init);
  }

  void _init(InitEvent event, Emitter<AlbumState> emit) {
    emit(AlbumState.loaded(event.album));
  }
}
