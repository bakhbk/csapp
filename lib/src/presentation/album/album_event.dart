import 'package:csapp/src/domain/entities/album.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'album_event.freezed.dart';

@freezed
class AlbumEvent with _$AlbumEvent {
  const factory AlbumEvent.init(Album album) = InitEvent;
}
