import 'package:csapp/src/core/utils/string.dart';
import 'package:csapp/src/di/injector.dart';
import 'package:csapp/src/domain/entities/album.dart';
import 'package:csapp/src/presentation/album/album_bloc.dart';
import 'package:csapp/src/presentation/album/album_event.dart';
import 'package:csapp/src/presentation/album/album_state.dart';
import 'package:csapp/src/presentation/widgets/image_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:skeletons/skeletons.dart';

class AlbumPage extends StatelessWidget {
  final Album album;

  const AlbumPage({required this.album, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(album.title.capitalize())),
      body: BlocProvider(
        create: (_) => getIt<AlbumBloc>()..add(AlbumEvent.init(album)),
        child: _buildPage(),
      ),
    );
  }

  Widget _buildPage() {
    return BlocBuilder<AlbumBloc, AlbumState>(
      builder: (context, state) {
        if (state is LoadingState) return const SkeletonAvatar();
        if (state is LoadedState) {
          final width = MediaQuery.of(context).size.width - 36;
          final textTheme = Theme.of(context).textTheme;
          final photos = album.photos;
          return PageView.builder(
            itemCount: photos.length,
            controller: PageController(viewportFraction: 0.9),
            itemBuilder: (_, i) {
              final photo = photos[i];
              return Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    const SizedBox(height: 16),
                    Flexible(
                      child: ImageView(
                        src: photo.url,
                        size: Size.fromWidth(width),
                      ),
                    ),
                    const SizedBox(height: 16),
                    Text(
                      photo.title,
                      style: textTheme.headline5,
                      textAlign: TextAlign.center,
                    ),
                    const SizedBox(height: 16),
                  ],
                ),
              );
            },
          );
        }
        return const SizedBox();
      },
    );
  }
}
