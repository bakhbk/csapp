import 'package:csapp/src/domain/entities/album.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'album_state.freezed.dart';

@freezed
class AlbumState with _$AlbumState {
  const factory AlbumState.loading() = LoadingState;

  const factory AlbumState.loaded(Album album) = LoadedState;
}
