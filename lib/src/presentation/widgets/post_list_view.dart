import 'package:csapp/src/domain/entities/post.dart';
import 'package:flutter/material.dart';

class PostListView extends StatelessWidget {
  final List<Post> posts;
  final Function(int index)? onSelectedItem;
  final ScrollPhysics? physics;

  const PostListView({
    required this.posts,
    this.onSelectedItem,
    this.physics,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      physics: physics,
      itemCount: posts.length,
      itemBuilder: (_, i) => _buttonWrap(
        i,
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 4),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Text(posts[i].title, maxLines: 1),
              Text(
                posts[i].body,
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
              const Divider(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buttonWrap(int i, Widget child) {
    final _onSelectedPost = onSelectedItem;
    if (_onSelectedPost == null) {
      return child;
    }

    return InkWell(
      onTap: () => _onSelectedPost(i),
      child: child,
    );
  }
}
