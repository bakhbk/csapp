import 'package:flutter/material.dart';
import 'package:skeletons/skeletons.dart';

class ImageView extends StatelessWidget {
  final String src;
  final Size size;

  const ImageView({
    required this.src,
    Size? size,
    Key? key,
  })  : size = size ?? const Size(48, 48),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return RepaintBoundary(
      child: Image.network(
        src,
        width: size.width,
        height: size.width,
        fit: BoxFit.contain,
        frameBuilder: (context, child, frame, wasSynchronouslyLoaded) {
          if (wasSynchronouslyLoaded) return child;
          return Skeleton(
            isLoading: frame == null,
            skeleton: Container(
              color: Theme.of(context).colorScheme.onBackground,
              width: size.width,
              height: size.width,
            ),
            child: child,
          );
        },
      ),
    );
  }
}
