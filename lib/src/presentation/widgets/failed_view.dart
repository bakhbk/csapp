import 'package:flutter/material.dart';

class FailedView extends StatelessWidget {
  final String text;

  const FailedView({Key? key, String? text})
      : text = text ?? 'Something went wrong',
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(child: Text(text));
  }
}
