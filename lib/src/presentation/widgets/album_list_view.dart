import 'package:csapp/src/domain/entities/album.dart';
import 'package:csapp/src/presentation/widgets/image_view.dart';
import 'package:flutter/material.dart';

class AlbumListView extends StatelessWidget {
  final List<Album> albums;
  final Function(int index)? onSelectedItem;
  final ScrollPhysics? physics;

  const AlbumListView({
    required this.albums,
    this.onSelectedItem,
    this.physics,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      physics: physics,
      itemCount: albums.length,
      itemBuilder: (_, i) {
        final photos = albums[i].photos.first;
        return _buttonWrap(
          i,
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 4),
            child: Row(
              children: [
                ImageView(src: photos.thumbnailUrl),
                const SizedBox(width: 8),
                Expanded(
                  child: Text(photos.title, maxLines: 1),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget _buttonWrap(int i, Widget child) {
    final _onSelectedPost = onSelectedItem;
    if (_onSelectedPost == null) {
      return child;
    }

    return InkWell(
      onTap: () => _onSelectedPost(i),
      child: child,
    );
  }
}
