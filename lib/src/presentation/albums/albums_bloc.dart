import 'dart:async';

import 'package:csapp/src/config/routes/routes.gr.dart';
import 'package:csapp/src/core/params/request_params.dart';
import 'package:csapp/src/core/resources/data_state.dart';
import 'package:csapp/src/core/utils/constants.dart';
import 'package:csapp/src/domain/use_cases/get_album_list_use_case.dart';
import 'package:csapp/src/domain/use_cases/get_albums_by_user_id_use_case.dart';
import 'package:csapp/src/presentation/albums/albums_event.dart';
import 'package:csapp/src/presentation/albums/albums_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class AlbumsBloc extends Bloc<AlbumsEvent, AlbumsState> {
  final GetAlbumsUseCase _getAlbumsUseCase;
  final GetAlbumsByUserIdUseCase _getAlbumsByUserIdUseCase;
  final AppRouter _router;

  AlbumsBloc(
    this._getAlbumsUseCase,
    this._getAlbumsByUserIdUseCase,
    this._router,
  ) : super(const AlbumsState.loading()) {
    on<InitEvent>(_init);
    on<OpenAlbumByIdEvent>(_openSelectedAlbum);
  }

  Future<void> _init(InitEvent event, Emitter<AlbumsState> emit) async {
    try {
      final dataState = event.userId == kEmptyUserId
          ? await _getAlbumsUseCase.call()
          : await _getAlbumsByUserIdUseCase.call(
              AlbumsRequestParams(userId: event.userId),
            );
      emit(AlbumsState.loaded(dataState.data));
    } on DataFailed {
      emit(const AlbumsState.loadingFailed());
    }
  }

  FutureOr<void> _openSelectedAlbum(OpenAlbumByIdEvent event, _) async {
    _router.push(AlbumPageRoute(album: event.album));
  }
}
