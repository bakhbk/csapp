import 'package:csapp/src/domain/entities/album.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'albums_state.freezed.dart';

@freezed
class AlbumsState with _$AlbumsState {
  const factory AlbumsState.loading() = LoadingState;

  const factory AlbumsState.loaded(List<Album> albums) = LoadedState;

  const factory AlbumsState.loadingFailed() = LoadingFailedState;
}
