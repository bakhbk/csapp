import 'package:csapp/src/domain/entities/album.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'albums_event.freezed.dart';

@freezed
class AlbumsEvent with _$AlbumsEvent {
  const factory AlbumsEvent.init(int userId) = InitEvent;

  const factory AlbumsEvent.openPost(Album album) = OpenAlbumByIdEvent;
}
