import 'package:csapp/src/core/utils/constants.dart';
import 'package:csapp/src/di/injector.dart';
import 'package:csapp/src/presentation/albums/albums_bloc.dart';
import 'package:csapp/src/presentation/albums/albums_event.dart';
import 'package:csapp/src/presentation/albums/albums_state.dart';
import 'package:csapp/src/presentation/widgets/album_list_view.dart';
import 'package:csapp/src/presentation/widgets/failed_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:skeletons/skeletons.dart';

class RootAlbumsPage extends StatelessWidget {
  const RootAlbumsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const AlbumsPage(userId: kEmptyUserId);
  }
}

class AlbumsPage extends StatelessWidget {
  final int userId;

  const AlbumsPage({required this.userId, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: userId != kEmptyUserId,
        title: const Text('Albums'),
      ),
      body: BlocProvider(
        create: (BuildContext context) =>
            getIt<AlbumsBloc>()..add(AlbumsEvent.init(userId)),
        child: Builder(builder: _buildPage),
      ),
    );
  }

  Widget _buildPage(BuildContext context) {
    final bloc = BlocProvider.of<AlbumsBloc>(context);

    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: BlocBuilder<AlbumsBloc, AlbumsState>(
        builder: (context, state) {
          if (state is LoadingState) return SkeletonListView();
          if (state is LoadedState) {
            return AlbumListView(
              albums: state.albums,
              onSelectedItem: (index) => bloc.add(
                AlbumsEvent.openPost(state.albums[index]),
              ),
            );
          }
          if (state is LoadingFailedState) return const FailedView();
          return const SizedBox();
        },
      ),
    );
  }
}
