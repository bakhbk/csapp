// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'albums_event.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$AlbumsEventTearOff {
  const _$AlbumsEventTearOff();

  InitEvent init(int userId) {
    return InitEvent(
      userId,
    );
  }

  OpenAlbumByIdEvent openPost(Album album) {
    return OpenAlbumByIdEvent(
      album,
    );
  }
}

/// @nodoc
const $AlbumsEvent = _$AlbumsEventTearOff();

/// @nodoc
mixin _$AlbumsEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(int userId) init,
    required TResult Function(Album album) openPost,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(int userId)? init,
    TResult Function(Album album)? openPost,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(int userId)? init,
    TResult Function(Album album)? openPost,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitEvent value) init,
    required TResult Function(OpenAlbumByIdEvent value) openPost,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitEvent value)? init,
    TResult Function(OpenAlbumByIdEvent value)? openPost,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitEvent value)? init,
    TResult Function(OpenAlbumByIdEvent value)? openPost,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AlbumsEventCopyWith<$Res> {
  factory $AlbumsEventCopyWith(
          AlbumsEvent value, $Res Function(AlbumsEvent) then) =
      _$AlbumsEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$AlbumsEventCopyWithImpl<$Res> implements $AlbumsEventCopyWith<$Res> {
  _$AlbumsEventCopyWithImpl(this._value, this._then);

  final AlbumsEvent _value;
  // ignore: unused_field
  final $Res Function(AlbumsEvent) _then;
}

/// @nodoc
abstract class $InitEventCopyWith<$Res> {
  factory $InitEventCopyWith(InitEvent value, $Res Function(InitEvent) then) =
      _$InitEventCopyWithImpl<$Res>;
  $Res call({int userId});
}

/// @nodoc
class _$InitEventCopyWithImpl<$Res> extends _$AlbumsEventCopyWithImpl<$Res>
    implements $InitEventCopyWith<$Res> {
  _$InitEventCopyWithImpl(InitEvent _value, $Res Function(InitEvent) _then)
      : super(_value, (v) => _then(v as InitEvent));

  @override
  InitEvent get _value => super._value as InitEvent;

  @override
  $Res call({
    Object? userId = freezed,
  }) {
    return _then(InitEvent(
      userId == freezed
          ? _value.userId
          : userId // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$InitEvent implements InitEvent {
  const _$InitEvent(this.userId);

  @override
  final int userId;

  @override
  String toString() {
    return 'AlbumsEvent.init(userId: $userId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is InitEvent &&
            const DeepCollectionEquality().equals(other.userId, userId));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(userId));

  @JsonKey(ignore: true)
  @override
  $InitEventCopyWith<InitEvent> get copyWith =>
      _$InitEventCopyWithImpl<InitEvent>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(int userId) init,
    required TResult Function(Album album) openPost,
  }) {
    return init(userId);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(int userId)? init,
    TResult Function(Album album)? openPost,
  }) {
    return init?.call(userId);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(int userId)? init,
    TResult Function(Album album)? openPost,
    required TResult orElse(),
  }) {
    if (init != null) {
      return init(userId);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitEvent value) init,
    required TResult Function(OpenAlbumByIdEvent value) openPost,
  }) {
    return init(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitEvent value)? init,
    TResult Function(OpenAlbumByIdEvent value)? openPost,
  }) {
    return init?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitEvent value)? init,
    TResult Function(OpenAlbumByIdEvent value)? openPost,
    required TResult orElse(),
  }) {
    if (init != null) {
      return init(this);
    }
    return orElse();
  }
}

abstract class InitEvent implements AlbumsEvent {
  const factory InitEvent(int userId) = _$InitEvent;

  int get userId;
  @JsonKey(ignore: true)
  $InitEventCopyWith<InitEvent> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $OpenAlbumByIdEventCopyWith<$Res> {
  factory $OpenAlbumByIdEventCopyWith(
          OpenAlbumByIdEvent value, $Res Function(OpenAlbumByIdEvent) then) =
      _$OpenAlbumByIdEventCopyWithImpl<$Res>;
  $Res call({Album album});

  $AlbumCopyWith<$Res> get album;
}

/// @nodoc
class _$OpenAlbumByIdEventCopyWithImpl<$Res>
    extends _$AlbumsEventCopyWithImpl<$Res>
    implements $OpenAlbumByIdEventCopyWith<$Res> {
  _$OpenAlbumByIdEventCopyWithImpl(
      OpenAlbumByIdEvent _value, $Res Function(OpenAlbumByIdEvent) _then)
      : super(_value, (v) => _then(v as OpenAlbumByIdEvent));

  @override
  OpenAlbumByIdEvent get _value => super._value as OpenAlbumByIdEvent;

  @override
  $Res call({
    Object? album = freezed,
  }) {
    return _then(OpenAlbumByIdEvent(
      album == freezed
          ? _value.album
          : album // ignore: cast_nullable_to_non_nullable
              as Album,
    ));
  }

  @override
  $AlbumCopyWith<$Res> get album {
    return $AlbumCopyWith<$Res>(_value.album, (value) {
      return _then(_value.copyWith(album: value));
    });
  }
}

/// @nodoc

class _$OpenAlbumByIdEvent implements OpenAlbumByIdEvent {
  const _$OpenAlbumByIdEvent(this.album);

  @override
  final Album album;

  @override
  String toString() {
    return 'AlbumsEvent.openPost(album: $album)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is OpenAlbumByIdEvent &&
            const DeepCollectionEquality().equals(other.album, album));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(album));

  @JsonKey(ignore: true)
  @override
  $OpenAlbumByIdEventCopyWith<OpenAlbumByIdEvent> get copyWith =>
      _$OpenAlbumByIdEventCopyWithImpl<OpenAlbumByIdEvent>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(int userId) init,
    required TResult Function(Album album) openPost,
  }) {
    return openPost(album);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(int userId)? init,
    TResult Function(Album album)? openPost,
  }) {
    return openPost?.call(album);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(int userId)? init,
    TResult Function(Album album)? openPost,
    required TResult orElse(),
  }) {
    if (openPost != null) {
      return openPost(album);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitEvent value) init,
    required TResult Function(OpenAlbumByIdEvent value) openPost,
  }) {
    return openPost(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitEvent value)? init,
    TResult Function(OpenAlbumByIdEvent value)? openPost,
  }) {
    return openPost?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitEvent value)? init,
    TResult Function(OpenAlbumByIdEvent value)? openPost,
    required TResult orElse(),
  }) {
    if (openPost != null) {
      return openPost(this);
    }
    return orElse();
  }
}

abstract class OpenAlbumByIdEvent implements AlbumsEvent {
  const factory OpenAlbumByIdEvent(Album album) = _$OpenAlbumByIdEvent;

  Album get album;
  @JsonKey(ignore: true)
  $OpenAlbumByIdEventCopyWith<OpenAlbumByIdEvent> get copyWith =>
      throw _privateConstructorUsedError;
}
