part of 'editable_dialog.dart';

class _DialogHandler {
  final Map<String, List<VoidCallback>> _callbackMap;
  final StreamController<bool> _controller;
  final Map<String, dynamic> dataMap;

  _DialogHandler(List<String> keyList)
      : _callbackMap = {},
        dataMap = {for (final key in keyList) key: ''},
        _controller = StreamController<bool>();

  Stream<bool> get stream => _controller.stream;

  void notify(String key) {
    final callbackMap = _callbackMap[key];
    if (callbackMap is List && callbackMap != null) {
      for (final c in callbackMap) {
        c.call();
      }
    }
    _controller.add(_isAnyValueEmpty);
  }

  void subscribe(String key, Function() callback) {
    if (_callbackMap[key] is List) {
      _callbackMap[key]?.add(callback);
    } else {
      _callbackMap[key] = [callback];
    }
  }

  void add(String currentKey, String? value) {
    dataMap[currentKey] = value ?? '';
    _controller.add(_isAnyValueEmpty);
  }

  bool get _isAnyValueEmpty {
    return dataMap.values.any((value) => value.toString().isEmpty);
  }
}
