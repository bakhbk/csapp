import 'dart:async';

import 'package:auto_route/auto_route.dart';
import 'package:csapp/src/core/utils/string.dart';
import 'package:flutter/material.dart';

part 'dialog_handler.dart';

part 'text_form_field.dart';

void showEditableDialog({
  required String title,
  required BuildContext context,
  required Function(Map<String, dynamic> map) onPressed,
  List<String> keyList = const ['name', 'email', 'body'],
  String confirmButtonText = 'SEND',
  String cancelButtonText = 'CANCEL',
  int maxLength = 255,
}) {
  final handler = _DialogHandler(keyList);
  final themeData = Theme.of(context);
  final _router = AutoRouter.of(context);
  showModalBottomSheet(
    context: context,
    backgroundColor: Colors.transparent,
    builder: (_) {
      return Container(
        padding: const EdgeInsets.symmetric(horizontal: 16.0),
        margin: const EdgeInsets.all(16.0),
        decoration: BoxDecoration(
          color: themeData.dialogBackgroundColor.withOpacity(0.9),
          borderRadius: BorderRadius.circular(16),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            const SizedBox(height: 8),
            Text(title, style: themeData.textTheme.bodyText1),
            Flexible(
              child: ListView.builder(
                shrinkWrap: true,
                itemCount: keyList.length,
                itemBuilder: (_, i) => _TextFormField(
                  currentKey: keyList[i],
                  previousKey: i == 0 ? '' : keyList[i - 1],
                  handler: handler,
                ),
              ),
            ),
            const SizedBox(height: 8),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Expanded(
                  child: TextButton(
                    onPressed: _router.pop,
                    child: Text(
                      cancelButtonText,
                      style: themeData.textTheme.button,
                    ),
                  ),
                ),
                Expanded(
                  child: StreamBuilder<bool>(
                    stream: handler.stream,
                    initialData: true,
                    builder: (_, snapshot) {
                      final isSendButtonHidden = snapshot.data;
                      if (isSendButtonHidden == null) return const SizedBox();
                      return Visibility(
                        visible: !isSendButtonHidden,
                        child: TextButton(
                          onPressed: () {
                            onPressed(handler.dataMap);
                            _router.pop();
                          },
                          child: Text(
                            confirmButtonText,
                            style: themeData.textTheme.button,
                          ),
                        ),
                      );
                    },
                  ),
                ),
              ],
            ),
            const SizedBox(height: 8),
          ],
        ),
      );
    },
  );
}
