part of 'editable_dialog.dart';

class _TextFormField extends StatefulWidget {
  const _TextFormField({
    required this.currentKey,
    required this.previousKey,
    required this.handler,
    Key? key,
  }) : super(key: key);

  final _DialogHandler handler;
  final String currentKey;
  final String previousKey;

  @override
  State<_TextFormField> createState() => _TextFormFieldState();
}

class _TextFormFieldState extends State<_TextFormField> {
  final focusNode = FocusNode();

  @override
  void initState() {
    super.initState();
    widget.handler.subscribe(widget.previousKey, focusNode.requestFocus);
  }

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      focusNode: focusNode,
      autofocus: widget.previousKey.isEmpty,
      decoration: InputDecoration(labelText: widget.currentKey.capitalize()),
      onFieldSubmitted: (value) => value.isNotEmpty
          ? widget.handler.notify(widget.currentKey)
          : focusNode.requestFocus(),
      onChanged: (value) => widget.handler.add(
        widget.currentKey,
        value,
      ),
    );
  }

  @override
  void dispose() {
    focusNode.dispose();
    super.dispose();
  }
}
