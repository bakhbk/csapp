import 'package:csapp/src/domain/entities/post.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'posts_event.freezed.dart';

@freezed
class PostsEvent with _$PostsEvent {
  const factory PostsEvent.init(int userId) = InitEvent;

  const factory PostsEvent.openPost(Post post) = OpenPostByIdEvent;
}
