import 'package:csapp/src/domain/entities/post.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'posts_state.freezed.dart';

@freezed
class PostsState with _$PostsState {
  const factory PostsState.loading() = LoadingState;

  const factory PostsState.loaded(List<Post> posts) = LoadedState;

  const factory PostsState.loadingFailed() = LoadingFailedState;
}
