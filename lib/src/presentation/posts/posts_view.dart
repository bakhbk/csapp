import 'package:csapp/src/core/utils/constants.dart';
import 'package:csapp/src/di/injector.dart';
import 'package:csapp/src/presentation/posts/posts_bloc.dart';
import 'package:csapp/src/presentation/posts/posts_event.dart';
import 'package:csapp/src/presentation/posts/posts_state.dart';
import 'package:csapp/src/presentation/widgets/failed_view.dart';
import 'package:csapp/src/presentation/widgets/post_list_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:skeletons/skeletons.dart';

class RootPostsPage extends StatelessWidget {
  const RootPostsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const PostsPage(userId: kEmptyUserId);
  }
}

class PostsPage extends StatelessWidget {
  final int userId;

  const PostsPage({required this.userId, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: userId != kEmptyUserId,
        title: const Text('Posts'),
      ),
      body: BlocProvider(
        create: (_) => getIt<PostsBloc>()..add(PostsEvent.init(userId)),
        child: Builder(builder: (context) => _buildPage(context)),
      ),
    );
  }

  Widget _buildPage(BuildContext context) {
    final bloc = BlocProvider.of<PostsBloc>(context);

    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: BlocBuilder<PostsBloc, PostsState>(
        builder: (context, state) {
          if (state is LoadingState) return SkeletonListView();
          if (state is LoadedState) {
            return PostListView(
              posts: state.posts,
              onSelectedItem: (index) => bloc.add(
                PostsEvent.openPost(state.posts[index]),
              ),
            );
          }
          if (state is LoadingFailedState) return const FailedView();
          return const SizedBox();
        },
      ),
    );
  }
}
