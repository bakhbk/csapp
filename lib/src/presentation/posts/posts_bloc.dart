import 'dart:async';

import 'package:csapp/src/config/routes/routes.gr.dart';
import 'package:csapp/src/core/resources/data_state.dart';
import 'package:csapp/src/core/utils/constants.dart';
import 'package:csapp/src/domain/use_cases/get_post_list_by_user_id_use_case.dart';
import 'package:csapp/src/domain/use_cases/get_post_list_use_case.dart';
import 'package:csapp/src/presentation/posts/posts_event.dart';
import 'package:csapp/src/presentation/posts/posts_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class PostsBloc extends Bloc<PostsEvent, PostsState> {
  final GetPostListUseCase _getPostListUseCase;
  final GetPostsByUserIdUseCase _getPostsByUserIdUseCase;
  final AppRouter _router;

  PostsBloc(
    this._getPostListUseCase,
    this._getPostsByUserIdUseCase,
    this._router,
  ) : super(const PostsState.loading()) {
    on<OpenPostByIdEvent>(_openSelectedPost);
    on<InitEvent>(_init);
  }

  Future<void> _init(InitEvent event, Emitter<PostsState> emit) async {
    try {
      final dataState = event.userId == kEmptyUserId
          ? await _getPostListUseCase.call()
          : await _getPostsByUserIdUseCase.call(event.userId);
      emit(PostsState.loaded(dataState.data));
    } on DataFailed {
      emit(const PostsState.loadingFailed());
    }
  }

  FutureOr<void> _openSelectedPost(OpenPostByIdEvent event, _) async {
    await _router.push(PostPageRoute(post: event.post));
  }
}
