// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'posts_event.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$PostsEventTearOff {
  const _$PostsEventTearOff();

  InitEvent init(int userId) {
    return InitEvent(
      userId,
    );
  }

  OpenPostByIdEvent openPost(Post post) {
    return OpenPostByIdEvent(
      post,
    );
  }
}

/// @nodoc
const $PostsEvent = _$PostsEventTearOff();

/// @nodoc
mixin _$PostsEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(int userId) init,
    required TResult Function(Post post) openPost,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(int userId)? init,
    TResult Function(Post post)? openPost,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(int userId)? init,
    TResult Function(Post post)? openPost,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitEvent value) init,
    required TResult Function(OpenPostByIdEvent value) openPost,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitEvent value)? init,
    TResult Function(OpenPostByIdEvent value)? openPost,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitEvent value)? init,
    TResult Function(OpenPostByIdEvent value)? openPost,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PostsEventCopyWith<$Res> {
  factory $PostsEventCopyWith(
          PostsEvent value, $Res Function(PostsEvent) then) =
      _$PostsEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$PostsEventCopyWithImpl<$Res> implements $PostsEventCopyWith<$Res> {
  _$PostsEventCopyWithImpl(this._value, this._then);

  final PostsEvent _value;
  // ignore: unused_field
  final $Res Function(PostsEvent) _then;
}

/// @nodoc
abstract class $InitEventCopyWith<$Res> {
  factory $InitEventCopyWith(InitEvent value, $Res Function(InitEvent) then) =
      _$InitEventCopyWithImpl<$Res>;
  $Res call({int userId});
}

/// @nodoc
class _$InitEventCopyWithImpl<$Res> extends _$PostsEventCopyWithImpl<$Res>
    implements $InitEventCopyWith<$Res> {
  _$InitEventCopyWithImpl(InitEvent _value, $Res Function(InitEvent) _then)
      : super(_value, (v) => _then(v as InitEvent));

  @override
  InitEvent get _value => super._value as InitEvent;

  @override
  $Res call({
    Object? userId = freezed,
  }) {
    return _then(InitEvent(
      userId == freezed
          ? _value.userId
          : userId // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$InitEvent implements InitEvent {
  const _$InitEvent(this.userId);

  @override
  final int userId;

  @override
  String toString() {
    return 'PostsEvent.init(userId: $userId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is InitEvent &&
            const DeepCollectionEquality().equals(other.userId, userId));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(userId));

  @JsonKey(ignore: true)
  @override
  $InitEventCopyWith<InitEvent> get copyWith =>
      _$InitEventCopyWithImpl<InitEvent>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(int userId) init,
    required TResult Function(Post post) openPost,
  }) {
    return init(userId);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(int userId)? init,
    TResult Function(Post post)? openPost,
  }) {
    return init?.call(userId);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(int userId)? init,
    TResult Function(Post post)? openPost,
    required TResult orElse(),
  }) {
    if (init != null) {
      return init(userId);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitEvent value) init,
    required TResult Function(OpenPostByIdEvent value) openPost,
  }) {
    return init(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitEvent value)? init,
    TResult Function(OpenPostByIdEvent value)? openPost,
  }) {
    return init?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitEvent value)? init,
    TResult Function(OpenPostByIdEvent value)? openPost,
    required TResult orElse(),
  }) {
    if (init != null) {
      return init(this);
    }
    return orElse();
  }
}

abstract class InitEvent implements PostsEvent {
  const factory InitEvent(int userId) = _$InitEvent;

  int get userId;
  @JsonKey(ignore: true)
  $InitEventCopyWith<InitEvent> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $OpenPostByIdEventCopyWith<$Res> {
  factory $OpenPostByIdEventCopyWith(
          OpenPostByIdEvent value, $Res Function(OpenPostByIdEvent) then) =
      _$OpenPostByIdEventCopyWithImpl<$Res>;
  $Res call({Post post});

  $PostCopyWith<$Res> get post;
}

/// @nodoc
class _$OpenPostByIdEventCopyWithImpl<$Res>
    extends _$PostsEventCopyWithImpl<$Res>
    implements $OpenPostByIdEventCopyWith<$Res> {
  _$OpenPostByIdEventCopyWithImpl(
      OpenPostByIdEvent _value, $Res Function(OpenPostByIdEvent) _then)
      : super(_value, (v) => _then(v as OpenPostByIdEvent));

  @override
  OpenPostByIdEvent get _value => super._value as OpenPostByIdEvent;

  @override
  $Res call({
    Object? post = freezed,
  }) {
    return _then(OpenPostByIdEvent(
      post == freezed
          ? _value.post
          : post // ignore: cast_nullable_to_non_nullable
              as Post,
    ));
  }

  @override
  $PostCopyWith<$Res> get post {
    return $PostCopyWith<$Res>(_value.post, (value) {
      return _then(_value.copyWith(post: value));
    });
  }
}

/// @nodoc

class _$OpenPostByIdEvent implements OpenPostByIdEvent {
  const _$OpenPostByIdEvent(this.post);

  @override
  final Post post;

  @override
  String toString() {
    return 'PostsEvent.openPost(post: $post)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is OpenPostByIdEvent &&
            const DeepCollectionEquality().equals(other.post, post));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(post));

  @JsonKey(ignore: true)
  @override
  $OpenPostByIdEventCopyWith<OpenPostByIdEvent> get copyWith =>
      _$OpenPostByIdEventCopyWithImpl<OpenPostByIdEvent>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(int userId) init,
    required TResult Function(Post post) openPost,
  }) {
    return openPost(post);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(int userId)? init,
    TResult Function(Post post)? openPost,
  }) {
    return openPost?.call(post);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(int userId)? init,
    TResult Function(Post post)? openPost,
    required TResult orElse(),
  }) {
    if (openPost != null) {
      return openPost(post);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InitEvent value) init,
    required TResult Function(OpenPostByIdEvent value) openPost,
  }) {
    return openPost(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(InitEvent value)? init,
    TResult Function(OpenPostByIdEvent value)? openPost,
  }) {
    return openPost?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InitEvent value)? init,
    TResult Function(OpenPostByIdEvent value)? openPost,
    required TResult orElse(),
  }) {
    if (openPost != null) {
      return openPost(this);
    }
    return orElse();
  }
}

abstract class OpenPostByIdEvent implements PostsEvent {
  const factory OpenPostByIdEvent(Post post) = _$OpenPostByIdEvent;

  Post get post;
  @JsonKey(ignore: true)
  $OpenPostByIdEventCopyWith<OpenPostByIdEvent> get copyWith =>
      throw _privateConstructorUsedError;
}
