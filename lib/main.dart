import 'dart:async';

import 'package:csapp/src/core/utils/logger.dart';
import 'package:csapp/src/di/injector.dart';
import 'package:csapp/src/presentation/app/app.dart';
import 'package:flutter/material.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await configureDependencies();
  runZonedGuarded(() => runApp(const App()), appErrors);
}
